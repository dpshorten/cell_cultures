import numpy as np
import h5py
import seaborn as sns
import os
import matplotlib.pyplot as plt
import matplotlib.colors as clr

from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import params

CULTURE_LABELS_TOP_GAP = 0.25
CULTURE_LABELS_GAP = 0.275

def plot_heatmaps(TE, sub_base_folder):

    fig, axs = utils.make_basic(True, trim_first_column = True)

    plt.subplots_adjust(hspace = 0.2, wspace = 0.05, left = 0.15)


    sm = plt.cm.ScalarMappable(cmap = 'viridis', norm=plt.Normalize(vmin=0, vmax=1))
    sm.set_array([])
    axins = inset_axes(axs[0, 2],
                   width="20%",  # width = 5% of parent_bbox width
                   height="100%",  # height : 50%
                   loc='lower left',
                   bbox_to_anchor=(0.2, 0, 0.5, 1),
                   bbox_transform=axs[0, 2].transAxes,
                   borderpad=0.0,
                   )
    cbar = fig.colorbar(sm, cax = axins, ticks = [0, 1])
    cbar.ax.set_yticklabels(['low TE', 'high TE'])

    for run_index in range(params.NUM_RUNS):
        for day_index in range(1, params.MAX_NUM_DAYS):
            if day_index >= len(params.DAYS[run_index]):
                axs[run_index, day_index - 1].axis('off')

            else:
                corrected_TE = TE[run_index, day_index, :, :]

                # sns.heatmap(corrected_TE, vmin = 1e-3, cmap = 'viridis', norm = clr.LogNorm())
                im = axs[run_index, day_index - 1].imshow(corrected_TE, cmap = 'viridis', norm = clr.LogNorm())
                axs[run_index, day_index - 1].set_facecolor('black')
                axs[run_index, day_index - 1].set_xticks([])
                axs[run_index, day_index - 1].set_yticks([])
                axs[run_index, day_index - 1].set_title("day " + str(params.DAYS[run_index][day_index]), y = -0.2)

    plt.savefig(params.BASE_FIGURE_FOLDER + sub_base_folder + params.NETS_AND_MAPS_FIGURE_FOLDER + "heatmaps.pdf")

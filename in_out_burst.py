import numpy as np
import h5py
import seaborn as sns
import os
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import pandas as pd

import params

OUTPUT_FILE_NAMES = [
    "fancy_edge_TE.pdf",
    "fancy_outgoing_TE.pdf",
    "fancy_outdegee.pdf",
    "fancy_incoming_TE.pdf",
    "fancy_indegee.pdf",
    "fancy_total_TE.pdf"
]

def plot_fancy(corrected_TE_list, butst_position, type):
    X = []
    Y = []
    sizes = []
    hues = []
    for run_index in range(len(params.RUNS)):
        corrected_TE = np.array(corrected_TE_list[run_index])

        data = np.array([])
        if type == 1:
            data = np.reshape(corrected_TE, (corrected_TE.shape[0],  3600))
        elif type == 2:
            data = np.mean(corrected_TE, axis = 2)
        elif type == 3:
            data = np.count_nonzero(corrected_TE, axis = 2)
        elif type == 4:
            data = np.mean(corrected_TE, axis = 1)
        elif type == 5:
            data = np.count_nonzero(corrected_TE, axis = 1)
        elif type == 6:
            data = np.mean(corrected_TE, axis = 1) + np.mean(corrected_TE, axis = 2)

        for i in range(len(params.DAYS[run_index])):
            for j in range(i + 1, len(params.DAYS[run_index])):
                if np.count_nonzero(data[i, :]) >= 1 and np.count_nonzero(data[j, :]) >= 1:
                    X.append(int(params.DAYS[run_index][i]))
                    Y.append(int(params.DAYS[run_index][j]))
                    #sizes.append(50 * (0.05 + np.corrcoef(data[i, :], data[j, :])[0, 1]))
                    sizes.append(1000)
                    hues.append(np.corrcoef(data[i, :], data[j, :])[0, 1])

    kw = dict(prop="colors", num=5)
    plt.rc('legend', fontsize=16)
    scatter = plt.scatter(X, Y, s = 100, cmap = plt.cm.plasma, c = hues)
    #plt.legend()
    handles, labels = scatter.legend_elements(**kw)
    legend2 = plt.legend(handles, labels, loc="lower right", title="correlation")
    # L=plt.legend()
    # L.get_texts()[0].set_text('Low\nCorrelation')
    # L.get_texts()[1].set_text('')
    # L.get_texts()[2].set_text('')
    # L.get_texts()[3].set_text('High\nCorrelation')
    plt.xlabel("Earlier Day")
    plt.ylabel("Later Day")

    plt.savefig(params.BASE_FIGURE_FOLDER + params.CORRELATIONS_OVER_TIME_FIGURE_FOLDER + OUTPUT_FILE_NAMES[type - 1], bbox_inches = 'tight')

    plt.clf()

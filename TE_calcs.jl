#import Pkg
#Pkg.add("Parameters")
#Pkg.add("Distances")
#Pkg.add("SpecialFunctions")
#Pkg.add("StatsBase")
#Pkg.add("CSV")
#Pkg.add("HDF5")
#Pkg.add("StaticArrays")

using Distributed

@everywhere include("jl_spike_train_utils.jl")

@everywhere using CSV
@everywhere using HDF5: h5open, create_group
#@everywhere using HDF5: h5open, g_create
@everywhere using Distances: Cityblock, Chebyshev, Euclidean
@everywhere using Base.Filesystem: walkdir
@everywhere using ArgParse
@everywhere using CoTETE

@everywhere L_Y = 2
@everywhere L_X = 4
@everywhere K = 10

@everywhere K_PERM = 4
@everywhere NUM_SURROGATES = 100
#@everywhere NUM_SURROGATES = 10

@everywhere NUM_SAMPLES_RATIO = 10.0
@everywhere SURROGATES_NUM_SAMPLES_RATIO = 10.0
@everywhere NUM_AVERAGE_SAMPLES = -1

@everywhere UPPER_INDEX = 60

@everywhere NUM_REPS_BEFORE_WORKER_RETURN = 10

@everywhere OBSERVATION_TIME_HOURS = 1
#@everywhere OBSERVATION_TIME_HOURS = 0.5
@everywhere observation_time_cycles = OBSERVATION_TIME_HOURS * 3600 * 25000
@everywhere TARGET_SPIKE_LIMIT = 10000

@everywhere struct EdgeRunSpec
    target_index::Int
    source_index::Int
end

@everywhere struct EdgeRunResult
    target_index::Int
    source_index::Int
    TE::AbstractFloat
    p::AbstractFloat
    surrogate_TE::Array{AbstractFloat}
    TE_in_burst::AbstractFloat
    surrogates_in_burst::Array{AbstractFloat}
    num_in_burst::Int
    TE_outside_burst::AbstractFloat
    surrogates_outside_burst::Array{AbstractFloat}
    num_outside_burst::Int
    iteration::Int
    pid::Int
end

@everywhere function TE_and_surrogate(spikes_filename, bursts_prefix_filename, jobs, results)

    #for i = 1:NUM_REPS_BEFORE_WORKER_RETURN
    while true
        run_spec = take!(jobs)
        p = 0
        TE = 0
        surrogates = zeros(NUM_SURROGATES)
        surrogates_in_burst = zeros(NUM_SURROGATES)
        surrogates_outside_burst = zeros(NUM_SURROGATES)
        TE_in_burst = 0
        TE_outside_burst = 0
        num_in_burst = 0
        num_outside_burst = 0

        target_spikes, source_spikes = read_target_and_source_from_csv(
            spikes_filename,
            run_spec.target_index,
            run_spec.source_index,
            UPPER_INDEX,
        )

        if (length(target_spikes) > 100 && length(source_spikes) > 100)

            target_spikes = target_spikes[target_spikes .<= (target_spikes[1] + observation_time_cycles)]
            source_spikes = source_spikes[source_spikes .<= target_spikes[end]]

            target_spikes = convert(Array{Float64,1}, target_spikes)
            target_spikes = target_spikes + (rand(length(target_spikes)) .- 0.5)
            target_spikes = sort(target_spikes)
            source_spikes = convert(Array{Float64,1}, source_spikes)
            source_spikes = source_spikes + (rand(length(source_spikes)) .- 0.5)
            source_spikes = sort(source_spikes)

        end

        if (
            run_spec.target_index != run_spec.source_index &&
            length(target_spikes) > 100 &&
            length(source_spikes) > 100
        )

            if length(target_spikes) > TARGET_SPIKE_LIMIT
                target_spikes = target_spikes[1:TARGET_SPIKE_LIMIT]
            end

            parameters = CoTETE.CoTETEParameters(
                l_x = L_X,
                l_y = L_Y,
                k_global = K,
                num_target_events_cap = -1,
                num_surrogates = NUM_SURROGATES,
                num_samples_ratio = NUM_SAMPLES_RATIO,
                surrogate_num_samples_ratio = SURROGATES_NUM_SAMPLES_RATIO,
                k_perm = K_PERM,
                num_average_samples = NUM_AVERAGE_SAMPLES,
                sampling_method = "jittered_target",
                jittered_sampling_noise = 200.0,
            )

            TE, p, surrogates, locals, surrogate_locals, event_times = CoTETE.estimate_TE_and_p_value_from_event_times(
                parameters,
                target_spikes,
                source_spikes,
                return_surrogate_TE_values = true,
                return_locals = true,
            )

            (TE_in_burst, surrogates_in_burst, num_in_burst,
             TE_outside_burst, surrogates_outside_burst, num_outside_burst) = separate_locals_into_burst_status(bursts_prefix_filename,
                                                                                                               locals, surrogate_locals,
                                                                                                               event_times)
        end
        sort!(surrogates)

        temp_result = EdgeRunResult(
            run_spec.target_index,
            run_spec.source_index,
            TE,
            p,
            surrogates,
            TE_in_burst,
            surrogates_in_burst,
            num_in_burst,
            TE_outside_burst,
            surrogates_outside_burst,
            num_outside_burst,
            0,
            myid(),
        )

        # put! will only work with a tuple. Not elegant, but it works
        put!(results, (temp_result, 1))
        finalize(temp_result)
        finalize(run_spec)
        target_spikes = 0
        source_spikes = 0
        GC.gc(true)
    end
    #spikes = 0
end

s = ArgParseSettings()
@add_arg_table s begin
    "--spike-file"
    help = "File with spike data"
    arg_type = String
    "--burst-pos-pref"
    help = "Prefix of burst position files"
    arg_type = String
    "--results-file"
    help = "File to output results"
    arg_type = String
    "--logs-file"
    help = "File write logs"
    arg_type = String
    "--num-splits"
    help = "Number of splits accross source nodes"
    arg_type = Int
    "--this-split"
    help = "The split the current run is working on"
    arg_type = Int
end
parsed_args = parse_args(ARGS, s)


@everywhere function edge_producer(jobs, bottom_source_index, top_source_index)
    for target_index = 1:UPPER_INDEX
        for source_index = bottom_source_index:top_source_index
            temp_run_spec = EdgeRunSpec(target_index, source_index)
            put!(jobs, temp_run_spec)
        end
    end
end

#println(root, " ", dirs, " ", files)
taskref = Ref{Task}()
jobs = RemoteChannel((taskref = taskref) -> Channel{EdgeRunSpec}(10))
results = RemoteChannel(() -> Channel{Any}(100))

# Create the producer
top_source_index = (parsed_args["this-split"] * Int(round(UPPER_INDEX / parsed_args["num-splits"])))
bottom_source_index =
    ((parsed_args["this-split"] - 1) * Int(round(UPPER_INDEX / parsed_args["num-splits"])) + 1)
remote_do(edge_producer, 2, jobs, bottom_source_index, top_source_index)
# Create the consumers
for i = 3:nworkers()
    remote_do(TE_and_surrogate, i, parsed_args["spike-file"], parsed_args["burst-pos-pref"], jobs, results)
end

logging_file = open(string(parsed_args["logs-file"], "-", parsed_args["this-split"], ".log"), "w")
results_file_name = string(parsed_args["results-file"], "-", parsed_args["this-split"], ".h5")
for i = 1:Int(round(((UPPER_INDEX^2) / parsed_args["num-splits"])))
    run_result, where = take!(results)
    println(run_result.target_index, " ", run_result.source_index)
    println(run_result.TE, " ", run_result.surrogates_in_burst)
    write(logging_file, string(run_result.target_index, " ", run_result.source_index, "\n"))
    flush(logging_file)
    h5open(results_file_name, isfile(results_file_name) ? "r+" : "w") do file
        g = create_group(file, string(run_result.target_index, " ", run_result.source_index))
        #g = g_create(file, string(run_result.target_index, " ", run_result.source_index))
        g["TE"] = run_result.TE
        g["TE_in_burst"] = run_result.TE_in_burst
        g["TE_outside_burst"] = run_result.TE_outside_burst
        g["num_in_burst"] = run_result.num_in_burst
        g["num_outside_burst"] = run_result.num_outside_burst
        g["p"] = run_result.p
        surrogates = Array{Float32}(run_result.surrogate_TE)
        surrogates_in_burst = Array{Float32}(run_result.surrogates_in_burst)
        surrogates_outside_burst = Array{Float32}(run_result.surrogates_outside_burst)
        g["surrogates"] = surrogates
        g["surrogates_in_burst"] = surrogates_in_burst
        g["surrogates_outside_burst"] = surrogates_outside_burst
        g["target_index"] = run_result.target_index
        g["source_index"] = run_result.source_index
    end
    GC.gc(true)
    #if run_result.iteration == NUM_REPS_BEFORE_WORKER_RETURN
    #    remote_do(TE_and_surrogate, run_result.pid, parsed_args["spike-file"], jobs, results)
    #end
end

rmprocs()

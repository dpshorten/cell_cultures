import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import networkx as nx
import statsmodels.api as sm

import params

# We remove based on the zero's in y
def make_a_plot(x, y, name, remove_zeros_on, sub_base_folder):

    fig, axs = utils.make_basic(True, trim_first_column = True)

    plt.subplots_adjust(hspace = 0.2, wspace = 0.2)

    for run_index in range(params.NUM_RUNS):
        for day_index in range(1, params.MAX_NUM_DAYS):
            #plt.clf()

            this_x = x[run_index, day_index, :, :]
            this_x = np.reshape(this_x, (this_x.shape[0], this_x.shape[1]))
            this_y = y[run_index, day_index, :, :]
            this_y = np.reshape(this_y, (this_y.shape[0], this_y.shape[1]))
            if remove_zeros_on == "x":
                this_y = this_y[np.logical_and(this_x != 0, this_x != -1)]
                this_x = this_x[np.logical_and(this_x != 0, this_x != -1)]
            else:
                this_x = this_x[np.logical_and(this_y != 0, this_y != -1)]
                this_y = this_y[np.logical_and(this_y != 0, this_y != -1)]


            if day_index >= len(params.DAYS[run_index]) or len(this_x) < 1 or len(this_y) < 1:
                axs[run_index, day_index - 1].axis('off')
            else:
                sns.regplot(this_x, this_y,
                    ax = axs[run_index, day_index - 1], line_kws={'linewidth':1}, scatter_kws={'s':1}, x_jitter = 0.0,
                    color = 'blue')
                axs[run_index, day_index - 1].set_xticks([])
                axs[run_index, day_index - 1].set_xlim((np.min(this_x), np.max(this_x)))
                axs[run_index, day_index - 1].set_ylim((np.min(this_y), np.max(this_y)))
                axs[run_index, day_index - 1].set_yticks([])

    plt.savefig(params.BASE_FIGURE_FOLDER + sub_base_folder + params.STATIC_CORRELATIONS_BY_EDGE_FIGURE_FOLDER + name + ".pdf")

def plot_correlations(TE, list_of_networks, spike_rates, mean_burst_positions, mean_precedences, sub_base_folder):

    #
    # source_total_TEs = []
    # target_total_TEs = []
    # source_in_TEs = []
    # target_in_TEs = []
    # source_out_TEs = []
    # target_out_TEs = []
    # source_cents = []
    # target_cents = []
    # TE_on_edge = []
    # target_in_degree = []
    # target_out_degree = []
    # source_in_degree = []
    # source_out_degree = []
    # target_rate = []
    # source_rate = []
    # burst_positions = []
    # precedences = []
    #
    # for container in [source_total_TEs, target_total_TEs, source_in_TEs, target_in_TEs, source_out_TEs, target_out_TEs,
    #                   source_cents, target_cents, TE_on_edge, target_in_degree, target_out_degree, source_in_degree, source_out_degree,
    #                   target_rate, source_rate, burst_positions, precedences]:
    #     for run_index in range(params.NUM_RUNS):
    #         temp = []
    #         for day_index in range(len(params.DAYS[run_index])):
    #             temp.append([])
    #         container.append(temp)
    #
    # for run_index in range(params.NUM_RUNS):
    #
    #     for day_index in range(len(params.DAYS[run_index])):
    #
    #         corrected_TE = np.array(corrected_TE_list[run_index][day_index])
    #
    #         meaned_in = np.mean(corrected_TE, axis = 0)
    #         meaned_out = np.mean(corrected_TE, axis = 1)
    #         total_TE = np.mean(corrected_TE, axis = 0) + np.mean(corrected_TE, axis = 1)
    #
    #         in_degree = np.count_nonzero(corrected_TE, axis = 0)
    #         out_degree = np.count_nonzero(corrected_TE, axis = 1)
    #         total_degree = np.count_nonzero(corrected_TE, axis = 0) + np.count_nonzero(corrected_TE, axis = 1)
    #
    #         temp_target_total_TE = []
    #         temp_source_total_TE = []
    #         temp_target_in_TE = []
    #         temp_source_in_TE = []
    #         temp_target_out_TE = []
    #         temp_source_out_TE = []
    #         temp_source_cents = []
    #         temp_target_cents = []
    #         temp_TE_on_edge = []
    #         temp_target_in_degree = []
    #         temp_target_out_degree = []
    #         temp_source_in_degree = []
    #         temp_source_out_degree = []
    #         temp_target_rate = []
    #         temp_source_rate = []
    #         temp_burst_positions = []
    #         temp_precedences = []
    #
    #
    #
    #         G = list_of_networks[run_index][day_index]
    #         cent_scores = []
    #         try:
    #             cent_scores = np.array([score for (node, score) in nx.eigenvector_centrality(G, tol = 1e-3, max_iter = 1000).items()])
    #         except:
    #             cent_scores = np.zeros(59)
    #         cent_scores = np.insert(cent_scores, 14, 0)
    #
    #         for i in range(corrected_TE.shape[0]):
    #             for j in range(corrected_TE.shape[1]):
    #                 if corrected_TE[i, j] != 0 and corrected_TE[i, j] <= np.mean(corrected_TE) + 4 * np.std(corrected_TE) and mean_precedences[run_index, day_index, i, j] != -1:
    #                 #if True:
    #                     temp_target_total_TE.append(total_TE[j])
    #                     temp_source_total_TE.append(total_TE[i])
    #                     temp_target_in_TE.append(meaned_in[j])
    #                     temp_source_in_TE.append(meaned_in[i])
    #                     temp_target_out_TE.append(meaned_out[j])
    #                     temp_source_out_TE.append(meaned_out[i])
    #                     temp_source_cents.append(cent_scores[i])
    #                     temp_target_cents.append(cent_scores[j])
    #                     temp_TE_on_edge.append(corrected_TE[i, j])
    #                     temp_target_in_degree.append(in_degree[j])
    #                     temp_target_out_degree.append(out_degree[j])
    #                     temp_source_in_degree.append(in_degree[i])
    #                     temp_source_out_degree.append(in_degree[i])
    #                     temp_source_rate.append(spike_rates[run_index, day_index, i])
    #                     temp_target_rate.append(spike_rates[run_index, day_index, j])
    #                     temp_burst_positions.append(mean_burst_positions[run_index][day_index][j] - mean_burst_positions[run_index][day_index][i])
    #                     temp_precedences.append(mean_precedences[run_index, day_index, i, j])
    #
    #
    #         source_total_TEs[run_index][day_index] = temp_source_total_TE
    #         target_total_TEs[run_index][day_index] = temp_target_total_TE
    #         source_in_TEs[run_index][day_index] = temp_source_in_TE
    #         target_in_TEs[run_index][day_index] = temp_target_in_TE
    #         source_out_TEs[run_index][day_index] = temp_source_out_TE
    #         target_out_TEs[run_index][day_index] = temp_target_out_TE
    #         source_cents[run_index][day_index] = temp_source_cents
    #         target_cents[run_index][day_index] = temp_target_cents
    #         TE_on_edge[run_index][day_index] = temp_TE_on_edge
    #         target_in_degree[run_index][day_index] = temp_target_in_degree
    #         target_out_degree[run_index][day_index] = temp_target_out_degree
    #         source_in_degree[run_index][day_index] = temp_source_in_degree
    #         source_out_degree[run_index][day_index] = temp_source_out_degree
    #         source_rate[run_index][day_index] = temp_source_rate
    #         target_rate[run_index][day_index] = temp_target_rate
    #         burst_positions[run_index][day_index] = temp_burst_positions
    #         precedences[run_index][day_index] = temp_precedences

    target_out_TE = np.mean(TE, axis = 2)
    target_out_TE = np.expand_dims(target_out_TE, axis = 3)
    target_out_TE = np.tile(target_out_TE, (1, 1, 1, target_out_TE.shape[2]))
    source_in_TE = np.mean(TE, axis = 3)
    source_in_TE = np.expand_dims(source_in_TE, axis = 2)
    source_in_TE = np.tile(source_in_TE, (1, 1, source_in_TE.shape[3], 1))
    make_a_plot(mean_precedences, TE, "precedence_vs_TE_on_edge", "y", sub_base_folder)
    make_a_plot(TE, target_out_TE, "TE_on_edge_vs_target_out_TE", "x", sub_base_folder)
    make_a_plot(TE, source_in_TE, "TE_on_edge_vs_source_in_TE", "x", sub_base_folder)

    # make_a_plot(burst_positions, TE_on_edge, "burst_pos_diff_vs_TE_on_edge")
    # make_a_plot(source_rate, TE_on_edge, "source_rate_vs_TE_on_edge")
    # make_a_plot(target_rate, TE_on_edge, "target_rate_vs_TE_on_edge")
    #
    # make_a_plot(target_total_TEs, source_total_TEs, "target_total_TE_vs_source_total_TE")
    # make_a_plot(target_in_TEs, source_in_TEs, "target_in_TE_vs_source_in_TE")
    # make_a_plot(target_out_TEs, source_out_TEs, "target_out_TE_vs_source_out_TE")
    # make_a_plot(source_out_TEs, target_out_TEs, "source_out_TE_vs_target_out_TE")
    # make_a_plot(target_in_TEs, source_out_TEs, "target_in_TE_vs_source_out_TE")
    # make_a_plot(target_out_TEs, source_in_TEs, "target_out_TE_vs_source_in_TE")
    #
    # make_a_plot(target_out_TEs, target_out_degree, "target_out_TE_vs_target_out_degree")
    # make_a_plot(target_in_TEs, target_in_degree, "target_in_TE_vs_target_in_degree")
    #
    # make_a_plot(source_out_TEs, source_out_degree, "source_out_TE_vs_source_out_degree")
    # make_a_plot(source_in_TEs, source_in_degree, "source_in_TE_vs_source_in_degree")
    #
    # make_a_plot(target_cents, source_total_TEs, "target_cent_vs_source_total_TE")
    # make_a_plot(target_total_TEs, source_cents, "target_total_TE_vs_source_cent")
    #
    # make_a_plot(TE_on_edge, source_cents, "TE_on_edge_vs_source_cent")
    # make_a_plot(TE_on_edge, target_cents, "TE_on_edge_vs_target_cent")
    # make_a_plot(TE_on_edge, target_in_TEs, "TE_on_edge_vs_target_in_TE")
    # make_a_plot(TE_on_edge, target_out_TEs, "TE_on_edge_vs_target_out_TE")
    # make_a_plot(TE_on_edge, source_in_TEs, "TE_on_edge_vs_source_in_TE")
    # make_a_plot(TE_on_edge, source_out_TEs, "TE_on_edge_vs_source_out_TE")
    # make_a_plot(TE_on_edge, source_in_degree, "TE_on_edge_vs_source_indegree")
    # make_a_plot(TE_on_edge, source_out_degree, "TE_on_edge_vs_source_outdegree")
    # make_a_plot(TE_on_edge, target_in_degree, "TE_on_edge_vs_target_indegree")
    # make_a_plot(TE_on_edge, target_out_degree, "TE_on_edge_vs_target_outdegree")

#using Plots

#NUM_TO_READ = Int(1e5)
INPUT_FILE = "../raw_data_wagenaar/1-1/1-1-14.1.spike"
using HDF5: h5open, g_create, attrs

struct SpikeInfo
    time::UInt64
    channel::Int16
    height::Int16
    width::Int16
    context::Array{Int16,1}
    threshold::Int16
end



function bar()
    f = open(INPUT_FILE, "r")


    potential = Float32[]

    time = Int64(read(f, UInt64))
    channel = read(f, Int16)
    height = read(f, Int16)
    width = read(f, Int16)
    context = 0
    for j = 1:74
        context = read(f, Int16)
    end
    threshold = read(f, Int16)

    start = time + 49
    prev_end_value = context * 0.33

    j = 1
    while !eof(f)
        time = Int64(read(f, UInt64))
        channel = read(f, Int16)
        height = read(f, Int16)
        width = read(f, Int16)
        contexts = zeros(74)
        for j = 1:74
            context = read(f, Int16)
            contexts[j] = context * 0.33
        end
        threshold = read(f, Int16)

        if channel == 1
            previous_time = length(potential) + start
            if (time - previous_time) <= 0
                println("CLANG")
                distance_in = previous_time - time
                append!(potential, contexts[(26 + distance_in):end])
            elseif (time - previous_time) < 26
                println("BANG")
                overlap = 26 - (time - previous_time)
                #for j = 1:overlap
                #    potential[end-overlap+j] = contexts[j]
                #end
                append!(potential, contexts[overlap:25])
                append!(potential, height)
                append!(potential, contexts[26:end])
            else
                println("time ", time)
                println("dist ", time - previous_time - 26)
                for i = 1:(time-previous_time-26)
                    push!(
                        potential,
                        0.33*randn() + prev_end_value + (i / (time - previous_time) * (contexts[1] - prev_end_value)),
                    )
                end
                append!(potential, contexts[1:25])
                println(start + length(potential), " ", time)
                append!(potential, height)
                append!(potential, contexts[26:end])
            end
            prev_end_value = contexts[end]
        end
    end
    potential = reshape(potential, (size(potential)...,1))
    h5open("test.h5", "w") do file
        #g = g_create(file, "foo")
        #attrs(g)["dtype"] = "Float32"
        #g["dtype"] = "Float32"
        #g["bar"] = potential
        write(file, "foo", potential)
    end

    #println(potential[1:100])
    #plotly()
    #display(plot(potential[2824579:(2824579+50000)]))
end

bar()

import numpy as np
import h5py
import seaborn as sns
import os
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.gridspec as gridspec
import pickle
import copy

import rate_scatter
import params
import heatmap_plotting
import layout_net
import dist_plotting
import box_and_scatter_plotting
import pairplots
import fancy_pairplot
import plain_network
import utils
import distance_TE_correlations
import node_in_vs_out
import edge_source_target_correlations
import distribution_summaries

def set_format():

    plt.rc('axes', titlesize=22)
    plt.rc('axes', labelsize=22)
    plt.rc('xtick', labelsize=18)
    plt.rc('ytick', labelsize=18)
    plt.rc('figure', titlesize=22)
    plt.rc('axes', linewidth=3)
    plt.rc('xtick.major', width=3)
    plt.rc('xtick.minor', width=3)
    plt.rc('ytick.major', width=3)
    plt.rc('ytick.minor', width=3)
    plt.rc('legend', fontsize=26)
    plt.rc('legend', handlelength=0.5)
    plt.rc('legend', borderpad=0.15)
    plt.rcParams['text.latex.preamble'] = [
       r'\usepackage{siunitx}',
       r'\sisetup{detect-all}',
       r'\usepackage{helvet}',
       r'\usepackage{sansmath}',
       r'\sansmath'
]
set_format()

pickle_file = open(params.PICKLED_DATA_FILE_NAME, "rb")
TE = pickle.load(pickle_file)
surrogates = pickle.load(pickle_file)
burst_TE = pickle.load(pickle_file)
burst_surrogates = pickle.load(pickle_file)
non_burst_TE = pickle.load(pickle_file)
non_burst_surrogates = pickle.load(pickle_file)
pickle_file.close()

for (vals, surrogates) in [(burst_TE, burst_surrogates), (TE, surrogates)]:
    utils.remove_dead_electrodes(vals, surrogates)
    utils.set_insignificants_to_zero(vals, surrogates)
    utils.bias_correct(vals, surrogates)

TE[0:11, :, :, :][TE[0:11, :, :, :] != -1] *= 2.5e4

list_of_networks = utils.build_networks(TE)
list_of_burst_networks = utils.build_networks(burst_TE)

mean_burst_positions, std_dev_burst_positions, mean_burst_times, mean_precedences = utils.get_mean_burst_positions()

for (TE_vals, networks, sub_base_folder) in [
                                             (TE, list_of_networks, params.NORMAL_SUB_BASE_FIGURE_FOLDER),
                                             (burst_TE, list_of_burst_networks, params.BURSTS_SUB_BASE_FIGURE_FOLDER),
                                            ]:
    #for i in [1, 2, 3, 5, 6]:
    #    fancy_pairplot.plot_fancy(TE_vals, sub_base_folder, type = i)

    for run_group_index in range(len(params.RUN_GROUPS)):

        #box_and_scatter_plotting.plot_box_and_scatter(TE_vals, sub_base_folder, run_group_index)

        dist_plotting.plot_distributions(TE_vals, sub_base_folder, run_group_index)

        #distribution_summaries.summarise_distributions(TE_vals)

        #

        #pairplots.make_pairplots(TE_vals, mean_burst_positions, sub_base_folder, remove_zeros = False)

        #edge_source_target_correlations.plot_correlations(TE_vals, networks, spike_rates,
        #                                                    mean_burst_positions, mean_precedences, sub_base_folder)

        #layout_net.construct_spatial_nets(networks, sub_base_folder, run_group_index)

        #node_in_vs_out.plot_in_vs_out_correlations(TE_vals, mean_burst_positions, run_group_index, sub_base_folder)


        #
        # heatmap_plotting.plot_heatmaps(TE_vals, sub_base_folder,)
        #
        # print(sub_base_folder)
        # distance_TE_correlations.plot_distance_TE_correlations(networks, TE_vals, sub_base_folder, include_zeros = True)
        # distance_TE_correlations.plot_distance_TE_correlations(networks, TE_vals, sub_base_folder, include_zeros = False)
        #
        #
        #not updated rate_scatter.plot_correlations(corrected_TE_list, list_of_networks, spike_rates)

        #not updated plain_network.make_network(spike_rates, "rates")
        #plain_network.make_network([], "nums")

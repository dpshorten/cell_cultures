from csv import reader, writer
from pathlib import Path

pathlist = Path('data_wagenaar/').glob('**/*.spk')
lengths = []
for path in pathlist:
    temp_length = []
    path_in_str = str(path)
    print(path_in_str)
    temp_length.append(path_in_str)
    f = open(path_in_str, 'r')

    csv_reader = reader(f,  delimiter=',')

    spikes = []
    for line in csv_reader:
        line = [int(time) for time in line if len(time) > 0]
        temp_length.append(len(line))

    lengths.append(temp_length)

print(lengths)

    
f = open('file_lengths.csv', 'w')
csv_writer = writer(f,  delimiter=',')
csv_writer.writerows(lengths)

using Distributed
using Random: randexp
using Statistics: mean
@everywhere using CSV
@everywhere using HDF5: h5open, g_create
@everywhere using Distances: Cityblock, Chebyshev, Euclidean
@everywhere using Base.Filesystem: walkdir
@everywhere using CoTETE

@everywhere L_X = 2
@everywhere K = 10

@everywhere NUM_SURROGATES = 10

@everywhere DATA_FILE_NAME = "extracted_data_wagenaar/1-3/1-3-24.2.spk"
@everywhere RESULTS_FILE_NAME = "results_wagenaar_AIS/1-3/1-3-24.2_HS_AIS.h5"
#@everywhere RESULTS_FILE_NAME = "AR_intervals.h5"

@everywhere struct NodeRunSpec
    target_index::Int
    l_x::Int
    spikes::Array{AbstractFloat,1}
end

@everywhere struct NodeRunResult
    target_index::Int
    l_x::Int
    AIS::AbstractFloat
    surrogate_AIS::Array{AbstractFloat}
    p::AbstractFloat
end

@everywhere function AIS_and_surrogates(jobs, results)

    while true
        run_spec = take!(jobs)
        AIS = 0
        p = 0
        surrogates = Float64[]
        if length(run_spec.spikes) > 50
            parameters = CoTETE.CoTETEParameters(
                l_x = run_spec.l_x,
                k_global = K,
                k_perm = 10,
                num_surrogates = NUM_SURROGATES,
                num_samples_ratio = 10.0,
                surrogate_num_samples_ratio = 10.0,
                num_target_events_cap = Int(1e4)
            )

            AIS, p, surrogates = CoTETE.estimate_AIS_and_p_value_of_last_dim_from_event_times(
                parameters,
                run_spec.spikes,
                return_surrogate_AIS_values = true,
            )
        end

        temp_result = NodeRunResult(run_spec.target_index, run_spec.l_x, AIS, surrogates, p)
        # put! will only work with a tuple. Not elegant, but it works
        put!(results, (temp_result, 1))

    end
end

@everywhere function node_producer(jobs, spikes)
    for target_index = 1:60
        for l_x = 2:5
            temp_run_spec = NodeRunSpec(target_index, l_x, spikes[target_index])
            put!(jobs, temp_run_spec)
        end
    end
end


#println(root, " ", dirs, " ", files)
taskref = Ref{Task}()
jobs = RemoteChannel((taskref = taskref) -> Channel{NodeRunSpec}(100))
results = RemoteChannel(() -> Channel{Any}(100))

#Create the producer
f = open(DATA_FILE_NAME)
lines = readlines(f)
spikes = []
for line in lines
    temp = Float64[]
    times = split(line, ",")
    for time in times
        if length(time) > 0
            push!(temp, parse(Float64, time))
        end
    end
    if (length(temp) > 0)
        temp = temp .- temp[1] .+ 1
    end
    temp = temp + (rand(length(temp)) .- 0.5)
    sort!(temp)
    push!(spikes, temp[10:(end-10)])
end

# println(spikes[1][1:20])
# foo = sort(spikes[1][2:end] - spikes[1][1:(end - 1)])
# println(foo[1:100])
# exit()

# spikes = []
# for j = 1:60
#     intervals = zeros(Float64, 10000)
#     auto_reg = 0
#     for i = 1:length(intervals)
#         auto_reg = 0.5 * auto_reg + randn()
#         intervals[i] = abs(auto_reg) + 0.1
#     end
#     temp = cumsum(intervals)
#     push!(spikes, temp)
# end


remote_do(node_producer, 2, jobs, spikes)
# Create the consumers
for i = 3:nworkers()
    remote_do(AIS_and_surrogates, i, jobs, results)
end


for i = 1:300
    run_result, where = take!(results)
    println(
        run_result.target_index,
        " ",
        run_result.l_x,
        " ",
        run_result.AIS,
        " ",
        run_result.p,
        " ",
        mean(run_result.surrogate_AIS),
    )
    println(run_result.surrogate_AIS)
    println()
    h5open(RESULTS_FILE_NAME, isfile(RESULTS_FILE_NAME) ? "r+" : "w") do file
        g = g_create(file, string(run_result.target_index, " ", run_result.l_x))
        g["AIS"] = run_result.AIS
        surrogates = Array{Float32}(run_result.surrogate_AIS)
        g["l_x"] = run_result.l_x
        g["surrogates"] = surrogates
        g["target_index"] = run_result.target_index
        g["p"] = run_result.p
    end
end

rmprocs()

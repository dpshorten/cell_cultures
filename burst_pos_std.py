import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import networkx as nx
import statsmodels.api as sm
import scipy.stats
from matplotlib.ticker import FormatStrFormatter
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import params
import utils

MARKER_SIZE = 3
COLOUR_MARKERS = sns.color_palette(palette = 'colorblind')[0]
COLOUR_LINES = sns.color_palette(palette = 'colorblind')[1]
MARKER_LINE_WIDTH = 4

def make_a_plot(x, y, name, sub_base_folder, xlabel, ylabel, colors = []):



    plt.rc('xtick', labelsize=10)
    plt.rc('ytick', labelsize=10)
    plt.rc('axes', linewidth=1.5)
    plt.rc('axes', labelsize=12)

    fig, axs = utils.make_basic(True, trim_first_column = False)

    plt.subplots_adjust(hspace = 0.35, wspace = 0.25)



    for run_index in range(params.NUM_RUNS):
        for day_index in range(params.MAX_NUM_DAYS):
            if day_index >= len(params.DAYS[run_index]) or np.count_nonzero(x[run_index][day_index, :]) < 5  or np.count_nonzero(y[run_index][day_index, :]) < 5:
                axs[run_index, day_index].axis('off')
            else:

                this_x = x[run_index][day_index, :]
                this_y = y[run_index][day_index, :]

                print("foo")
                print(type(x[run_index]))
                print(type(x[run_index][day_index, :]))
                print(type(this_x))
                print(this_x)

                combined_mask = this_x.mask | this_y.mask
                this_x = this_x[~combined_mask]
                this_y = this_y[~combined_mask]

                if len(this_x) == 0:
                    continue

                axs[run_index, day_index].yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
                axs[run_index, day_index].xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
                range_x = np.max(this_x) - np.min(this_x)
                range_y = np.max(this_y) - np.min(this_y)
                #axs[run_index, day_index].set_xlim([np.min(this_x) - 0.05 * range_x, np.max(this_x) + 0.05 * range_x])
                #axs[run_index, day_index].set_xlim([np.min(this_x) - 0.05 * range_x, np.max(this_x) + 0.05 * range_x])
                axs[run_index, day_index].set_xlim([0, 60.0])
                #axs[run_index, day_index].set_ylim([np.min(this_y) - 0.05 * range_y, np.max(this_y) + 0.05 * range_y])
                axs[run_index, day_index].set_ylim([0, 30.0])

                if len(colors) == 0:
                    axs[run_index, day_index].scatter(this_x, this_y, s = MARKER_SIZE, color = COLOUR_MARKERS)
                    # coef = np.polyfit(this_x, this_y, 1)
                    # line_fn = np.poly1d(coef)
                    # axs[run_index, day_index].plot(this_x, line_fn(this_x), color = COLOUR_LINES)
                    # rho, p_rho = scipy.stats.spearmanr(this_x, this_y)
                    # sig_string = ""
                    # if p_rho < 0.01/16:
                    #     sig_string = "**"
                    # elif p_rho < 0.05/16:
                    #     sig_string = "*"
                    #
                    # axs[run_index, day_index].text(np.min(this_x) + 0.1 * range_x, np.min(this_y) + 0.9 * range_y,
                    #          r'$\rho =' + "{:.2f}".format(rho) + r'$' + sig_string, fontdict = {'size' : 12})
                    # axs[run_index, day_index].text(np.min(this_x) + 0.1 * range_x, np.min(this_y) + 0.8 * range_y,
                    #          r'$p =' + "{:.2f}".format(p_rho) + r'$', fontdict = {'size' : 10})

                else:
                    these_colors = colors[run_index][day_index, :]
                    these_colors = these_colors[~combined_mask]
                    axs[run_index, day_index].scatter(this_x, this_y, c = these_colors, cmap = 'plasma')

                axs[run_index][day_index].set_xlabel("day " + str(params.DAYS[run_index][day_index]))

    #axs[3, 0].set_xlabel(xlabel + "\nday " + str(params.DAYS[3][0]))
    #axs[3, 0].set_ylabel(ylabel)

    # if len(colors) != 0:
    #     sm = plt.cm.ScalarMappable(cmap = 'plasma', norm=plt.Normalize(vmin=0, vmax=1))
    #     sm.set_array([])
    #     axins = inset_axes(axs[0, 3],
    #                width="20%",  # width = 5% of parent_bbox width
    #                height="100%",  # height : 50%
    #                loc='lower left',
    #                bbox_to_anchor=(0.2, 0, 0.5, 1),
    #                bbox_transform=axs[0, 3].transAxes,
    #                borderpad=0.0,
    #                )
    #     cbar = fig.colorbar(sm, cax = axins, ticks = [0, 1])
    #     cbar.ax.set_yticklabels(['early burster', 'late burster'])


    plt.savefig(params.BASE_FIGURE_FOLDER + sub_base_folder + params.STATIC_CORRELATIONS_BY_NODE_FIGURE_FOLDER + name + ".pdf")

def plot_in_vs_out_correlations(TE, list_of_networks, spike_rates, mean_burst_positions, sub_base_folder):

    meaned_in = []
    meaned_out = []

    # for run_index in range(params.NUM_RUNS):
    #
    #     this_runs_TE = TE[run_index, : , :, :]
    #
    #     this_runs_TE = np.ma.array(this_runs_TE, mask = this_runs_TE == -1)
    #     this_runs_TE[this_runs_TE == -1] = 0
    #     print(type(np.mean(this_runs_TE, axis = 1)))
    #     meaned_in.append(np.mean(this_runs_TE, axis = 1))
    #     print(type(meaned_in[run_index]))
    #     meaned_out.append(np.mean(this_runs_TE, axis = 2))
    #     print(type(meaned_out[run_index]))
    print(TE.shape)
    make_a_plot(mean_burst_positions, TE,  "mean_burst_position_vs_std_dev", sub_base_folder,
                "mean burst position", r'mean out TE (nats per spike)')

    # make_a_plot(meaned_out, meaned_in, "TE_out_vs_TE_in_vs_burst_pos", sub_base_folder,
    #             r'mean out TE (nats per spike)',
    #             r'mean in TE (nats per spike)',
    #             colors = mean_burst_positions)
    # # make_a_plot(mean_burst_positions, centralities,  "burst_position_vs_centrality", sub_base_folder)
    # make_a_plot(mean_burst_positions, meaned_in,  "burst_position_vs_normalised_TE_in", sub_base_folder,
    #             "mean burst position", r'mean in TE (nats per spike)')
    # make_a_plot(mean_burst_positions, meaned_out,  "burst_position_vs_normalised_TE_out", sub_base_folder,
    #             "mean burst position", r'mean out TE (nats per spike)')

import matplotlib.pyplot as plt
from csv import reader
import numpy as np
import cv2

import params

RESOLUTION = 20
TIME_FRACTION = 0.1

def read_spikes(reader):
    spikes = []
    #i = 0
    for line in reader:
        line = [int(time) for time in line if len(time) > 0]
        #i += 1
        #line = line[:10000]
        spikes.append(line)
    return spikes


f1 = open('extracted_data_wagenaar/2-5/2-5-28.1.spk', 'r')

csv_reader1 = reader(f1,  delimiter=',')

spikes1 = read_spikes(csv_reader1)

first_spike = np.min([spike_train[0] for spike_train in spikes1 if len(spike_train) > 0])
last_spike = np.max([spike_train[-1] for spike_train in spikes1 if len(spike_train) > 0])
num_frames = int(round((TIME_FRACTION * (last_spike - first_spike))/RESOLUTION))

for i in range(len(spikes1)):
    spikes1[i] = np.array(spikes1[i]) - first_spike + 1

latent_frames = np.zeros((num_frames, 16, 16), dtype = np.float64)
#frames = np.zeros((NUM_FRAMES, 8, 8), dtype = np.uint8)

next_spikes = np.zeros(60, dtype = np.int32)
for i in range(1, num_frames):
    latent_frames[i, :, :] = 0.9 * latent_frames[i - 1, :, :]
    for j in range(60):
        if len(spikes1[j]) > 0 and spikes1[j][next_spikes[j]] <= i * RESOLUTION:
            position = np.argwhere(params.ELECTRODE_POSITIONS == j)[0]
            latent_frames[i, 2 * position[0], 2 * position[1]] = 255
            while next_spikes[j] < len(spikes1[j]) and spikes1[j][next_spikes[j]] <= i * RESOLUTION:
                next_spikes[j] += 1
#for i in range(200):
#    print(frames[i])

frames = np.uint8(np.round(latent_frames))

out = cv2.VideoWriter('output_video3.mp4',cv2.VideoWriter_fourcc(*'MP4V'), 100, (16, 16), False)
for i in range(num_frames):
    out.write(frames[i, :, :])

out.release()

import numpy as np
import matplotlib.pyplot as plt
from csv import reader
import params

OBSERVATION_TIME_HOURS = 1
observation_time_cycles = OBSERVATION_TIME_HOURS * 3600 * 25000

def read_spikes(reader):
    spikes = []
    for line in reader:
        line = [int(time) for time in line if len(time) > 0]
        spikes.append(line)
    return spikes

all_times = []
for run_index in range(len(params.RUNS)):
    for day_index in range(len(params.DAYS[run_index])):
        f = open(params.BASE_SPIKE_TRAIN_FOLDER + params.RUNS[run_index] + "/" + params.ORIGINAL_SPIKE_TRAIN_FILES[run_index][day_index])
        csv_reader = reader(f,  delimiter=',')
        spikes = read_spikes(csv_reader)
        first_spikes = [train[0] for train in spikes if len(train) > 0]
        earliest_time = np.min(first_spikes)
        print("\n")
        print(params.ORIGINAL_SPIKE_TRAIN_FILES[run_index][day_index])
        times = np.zeros((len(spikes),))
        for i in range(len(spikes)):
            if len(spikes[i]) > 0:
                count = 0
                while count < len(spikes[i]) and spikes[i][count] < earliest_time + observation_time_cycles:
                    count += 1
                if count == len(spikes[i]):
                    print(i, count, "file end")
                else:
                    times[i] = count
            else:
                times[i] = 0
        times = np.sort(times)
        all_times.extend(times)
        print(len(times))
        print(times[0], times[1], times[2], times[3], times[30], times[56], times[-1])

print(np.max(times), np.median(times))

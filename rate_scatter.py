import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import networkx as nx
import statsmodels.api as sm

import params

def make_a_plot(x, y, name, colours = []):

    fig, axs = utils.make_basic(True, trim_first_column = True)

    plt.subplots_adjust(hspace = 0.2, wspace = 0.2)

    for run_index in range(params.NUM_RUNS):
        for day_index in range(1, params.MAX_NUM_DAYS):
            #plt.clf()
            if day_index >= len(params.DAYS[run_index]) or len(x[run_index][day_index]) < 1 or len(y[run_index][day_index]) < 1:
                axs[run_index, day_index - 1].axis('off')
            else:

                x[run_index][day_index] += np.random.normal(scale = 0.05*np.mean(x[run_index][day_index]), size = (len(x[run_index][day_index])))
                y[run_index][day_index] += np.random.normal(scale = 0.05*np.mean(y[run_index][day_index]), size = (len(y[run_index][day_index])))
                maxi = max(colours[run_index][day_index])
                mini = min(colours[run_index][day_index])
                diff = maxi - mini
                axs[run_index, day_index - 1].scatter(x[run_index][day_index], y[run_index][day_index],
                                              c = colours[run_index][day_index], vmin = mini, vmax = maxi - 0.3*diff,
                                              cmap = plt.cm.winter, s = 2, alpha = 0.7)
                                              #legend = False,
                axs[run_index, day_index - 1].set_xticks([])
                axs[run_index, day_index - 1].set_xlim((np.min(x[run_index][day_index]), np.max(x[run_index][day_index])))
                axs[run_index, day_index - 1].set_ylim((np.min(y[run_index][day_index]), np.max(y[run_index][day_index])))
                axs[run_index, day_index - 1].set_yticks([])

    plt.savefig(params.BASE_FIGURE_FOLDER + params.STATIC_CORRELATIONS_BY_EDGE_FIGURE_FOLDER + name + ".pdf")

def plot_correlations(corrected_TE_list, list_of_networks, spike_rates):

    source_total_TEs = []
    target_total_TEs = []
    source_in_TEs = []
    target_in_TEs = []
    source_out_TEs = []
    target_out_TEs = []
    source_cents = []
    target_cents = []
    TE_on_edge = []
    source_rates = []
    target_rates = []
    target_in_degree = []
    target_out_degree = []
    source_in_degree = []
    source_out_degree = []

    for container in [source_total_TEs, target_total_TEs, source_in_TEs, target_in_TEs, source_out_TEs, target_out_TEs,
                      source_cents, target_cents, TE_on_edge, target_in_degree, target_out_degree, source_in_degree, source_out_degree,
                      source_rates, target_rates]:
        for run_index in range(params.NUM_RUNS):
            temp = []
            for day_index in range(len(params.DAYS[run_index])):
                temp.append([])
            container.append(temp)

    for run_index in range(params.NUM_RUNS):

        for day_index in range(len(params.DAYS[run_index])):

            corrected_TE = np.array(corrected_TE_list[run_index][day_index])

            meaned_in = np.mean(corrected_TE, axis = 0)
            meaned_out = np.mean(corrected_TE, axis = 1)
            total_TE = np.mean(corrected_TE, axis = 0) + np.mean(corrected_TE, axis = 1)

            in_degree = np.count_nonzero(corrected_TE, axis = 0)
            out_degree = np.count_nonzero(corrected_TE, axis = 1)
            total_degree = np.count_nonzero(corrected_TE, axis = 0) + np.count_nonzero(corrected_TE, axis = 1)

            temp_target_total_TE = []
            temp_source_total_TE = []
            temp_target_in_TE = []
            temp_source_in_TE = []
            temp_target_out_TE = []
            temp_source_out_TE = []
            temp_source_cents = []
            temp_target_cents = []
            temp_TE_on_edge = []
            temp_target_in_degree = []
            temp_target_out_degree = []
            temp_source_in_degree = []
            temp_source_out_degree = []
            temp_source_rates = []
            temp_target_rates = []



            G = list_of_networks[run_index][day_index]
            cent_scores = []
            try:
                cent_scores = np.array([score for (node, score) in nx.eigenvector_centrality(G, tol = 1e-3, max_iter = 1000).items()])
            except:
                cent_scores = np.zeros(59)
            cent_scores = np.insert(cent_scores, 14, 0)

            for i in range(corrected_TE.shape[0]):
                for j in range(corrected_TE.shape[1]):
                    if corrected_TE[i, j] != 0:
                        temp_target_total_TE.append(total_TE[j])
                        temp_source_total_TE.append(total_TE[i])
                        temp_target_in_TE.append(meaned_in[j])
                        temp_source_in_TE.append(meaned_in[i])
                        temp_target_out_TE.append(meaned_out[j])
                        temp_source_out_TE.append(meaned_out[i])
                        temp_source_cents.append(cent_scores[i])
                        temp_target_cents.append(cent_scores[j])
                        temp_TE_on_edge.append(corrected_TE[i, j])
                        temp_target_in_degree.append(in_degree[j])
                        temp_target_out_degree.append(out_degree[j])
                        temp_source_in_degree.append(in_degree[i])
                        temp_source_out_degree.append(in_degree[i])
                        temp_source_rates.append(spike_rates[run_index][day_index, i])
                        temp_target_rates.append(spike_rates[run_index][day_index, j])


            source_total_TEs[run_index][day_index] = temp_source_total_TE
            target_total_TEs[run_index][day_index] = temp_target_total_TE
            source_in_TEs[run_index][day_index] = temp_source_in_TE
            target_in_TEs[run_index][day_index] = temp_target_in_TE
            source_out_TEs[run_index][day_index] = temp_source_out_TE
            target_out_TEs[run_index][day_index] = temp_target_out_TE
            source_cents[run_index][day_index] = temp_source_cents
            target_cents[run_index][day_index] = temp_target_cents
            TE_on_edge[run_index][day_index] = temp_TE_on_edge
            target_in_degree[run_index][day_index] = temp_target_in_degree
            target_out_degree[run_index][day_index] = temp_target_out_degree
            source_in_degree[run_index][day_index] = temp_source_in_degree
            source_out_degree[run_index][day_index] = temp_source_out_degree
            source_rates[run_index][day_index] = temp_source_rates
            target_rates[run_index][day_index] = temp_target_rates


    # make_a_plot(target_total_TEs, source_total_TEs, "target_total_TE_vs_source_total_TE")
    # make_a_plot(target_in_TEs, source_in_TEs, "target_in_TE_vs_source_in_TE")
    # make_a_plot(target_out_TEs, source_out_TEs, "target_out_TE_vs_source_out_TE")
    # make_a_plot(source_out_TEs, target_out_TEs, "source_out_TE_vs_target_out_TE")
    # make_a_plot(target_in_TEs, source_out_TEs, "target_in_TE_vs_source_out_TE")
    # make_a_plot(target_out_TEs, source_in_TEs, "target_out_TE_vs_source_in_TE")
    #
    # make_a_plot(target_out_TEs, target_out_degree, "target_out_TE_vs_target_out_degree")
    # make_a_plot(target_in_TEs, target_in_degree, "target_in_TE_vs_target_in_degree")
    #
    # make_a_plot(source_out_TEs, source_out_degree, "source_out_TE_vs_source_out_degree")
    # make_a_plot(source_in_TEs, source_in_degree, "source_in_TE_vs_source_in_degree")
    #
    # make_a_plot(target_cents, source_total_TEs, "target_cent_vs_source_total_TE")
    # make_a_plot(target_total_TEs, source_cents, "target_total_TE_vs_source_cent")
    #
    # make_a_plot(TE_on_edge, source_cents, "TE_on_edge_vs_source_cent")
    # make_a_plot(TE_on_edge, target_cents, "TE_on_edge_vs_target_cent")
    # make_a_plot(TE_on_edge, target_in_TEs, "TE_on_edge_vs_target_in_TE")
    # make_a_plot(TE_on_edge, target_out_TEs, "TE_on_edge_vs_target_out_TE")
    # make_a_plot(TE_on_edge, source_in_TEs, "TE_on_edge_vs_source_in_TE")
    # make_a_plot(TE_on_edge, source_out_TEs, "TE_on_edge_vs_source_out_TE")
    # make_a_plot(TE_on_edge, source_in_degree, "TE_on_edge_vs_source_indegree")
    # make_a_plot(TE_on_edge, source_out_degree, "TE_on_edge_vs_source_outdegree")
    # make_a_plot(TE_on_edge, target_in_degree, "TE_on_edge_vs_target_indegree")
    # make_a_plot(TE_on_edge, target_out_degree, "TE_on_edge_vs_target_outdegree")

    make_a_plot(source_rates, target_rates, "source_rate_target_rate_TE", colours = TE_on_edge)

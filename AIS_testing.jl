using CSV
using HDF5: h5open, g_create
using Distances: Cityblock, Chebyshev, Euclidean
using Base.Filesystem: walkdir
using Statistics: mean, std
using CoTETE

L_X = 5
K = 10

NUM_SURROGATES = 100

input_filename = "extracted_data_wagenaar/1-1/1-1-20.2.spk"
output_filename = "results_AIS/1-1/1-1-20.2_AIS.h5"

NUM_TARGET_EVENTS_CAP = Int(5e3)

f = open(string(input_filename))
lines = readlines(f)
spikes = []
for line in lines
    temp = Float64[]
    times = split(line, ",")
    for time in times
        if length(time) > 0
            push!(temp, parse(Float64, time))
        end
    end
    push!(spikes, temp)
end

for i = 1:length(spikes)
    TE, surrogates, p = CoTETE.estimate_AIS_and_surrogates(spikes[i], L_X, k_global = K)

    println(TE * 1e3, " ", mean(surrogates) * 1e3, " ", std(surrogates) * 1e3, " ", p)
end

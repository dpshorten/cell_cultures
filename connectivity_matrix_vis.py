import numpy as np
import h5py
from pathlib import Path
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import seaborn as sns

NUM_SURROGATES = 100
NUM_NODES = 60
DATA_FOLDER = "results_full/"
DATA_FILE_NAME = "2-2-33.2-ly1lx3u.h5"
FIGURE_FOLDER = "figures_wagenaar/2-2/"

TE = 1e-12 * np.zeros((NUM_NODES, NUM_NODES))
surrogates = np.zeros((NUM_NODES, NUM_NODES, NUM_SURROGATES))

data_file = h5py.File(DATA_FOLDER + DATA_FILE_NAME, 'r')

for key in data_file.keys():
    source_index = int(data_file[key]["source_index"].value)
    target_index = int(data_file[key]["target_index"].value)
    if data_file[key]["TE"].value != 0 and data_file[key]["p"].value < 0.05:
        TE[source_index - 1, target_index - 1] = data_file[key]["TE"].value
        surrogates[source_index - 1, target_index - 1, :] = data_file[key]["surrogates"].value
        #TE[source_index - 1, target_index - 1] = data_file[key]["TE"].value
        #surrogates[source_index - 1, target_index - 1, :] = data_file[key]["surrogates"].value


mean_surrogates = np.mean(surrogates, axis = 2)

corrected_TE = TE - mean_surrogates
corrected_TE *= 1e3
print(corrected_TE[1:5, 1:5])
#sns.heatmap(corrected_TE, vmin = 0, vmax = 0.1, cmap = 'viridis')
sns.heatmap(corrected_TE, vmin = 1e-3, cmap = 'viridis', norm = clr.LogNorm())
ax = plt.gca()
ax.set_facecolor('black')
plt.savefig(FIGURE_FOLDER + DATA_FILE_NAME[:-3] + ".png")
plt.show()


#plt.clf()

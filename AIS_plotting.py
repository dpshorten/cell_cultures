import numpy as np
import h5py
from pathlib import Path
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import scipy

NUM_NODES = 60
NUM_L_X = 5
NUM_SURROGATES = 10



AIS = np.zeros((NUM_L_X, NUM_NODES))
surrogates = np.zeros((NUM_L_X, NUM_NODES, NUM_SURROGATES))

data_file = h5py.File('results_wagenaar_AIS/1-3/1-3-24.2_HS_AIS.h5', 'r')
#data_file = h5py.File('AR_intervals.h5', 'r')

for key in data_file.keys():
    target_index = int(data_file[key]["target_index"].value)
    #print(target_index)
    l_x = data_file[key]["l_x"].value
    if isinstance(data_file[key]["surrogates"].value, np.ndarray):
        AIS[l_x - 1, target_index - 1] = data_file[key]["AIS"].value
        surrogates[l_x - 1, target_index - 1, :] = data_file[key]["surrogates"].value



AIS *= 2.5e4
surrogates *= 2.5e4

print(AIS[0, :])
print(AIS[1, :])

#print(AIS)
print(surrogates[0, 0, :])
mean_surrogates = np.mean(surrogates, axis = 2)
AIS -= mean_surrogates
#print(mean_surrogates)
#mean_surrogates = np.mean(surrogates, axis = 0)
#mean_AIS = np.mean(surrogates, axis = 1)

non_zero_AIS = []
for l_x in range(NUM_L_X):
    temp_non_zero_AIS = []
    for node in range(NUM_NODES):
        if AIS[l_x, node] != 0:
            temp_non_zero_AIS.append(AIS[l_x, node])
    print(len(temp_non_zero_AIS))
    non_zero_AIS.append(temp_non_zero_AIS)

#plt.boxplot(np.transpose(mean_surrogates[:, :]), showfliers = False)

print(np.mean(AIS, axis = 1))
print("\n\n\n")
for i in range(4):
    print("means ", np.mean(AIS[i]), " ", np.mean(AIS[i + 1]))
    print("stds ", np.std(AIS[i]), np.std(AIS[i + 1]))
    #print(len(TE[i]))
    print(scipy.stats.ttest_rel(AIS[i, :], AIS[i + 1, :], axis = None,
           alternative = 'less')[1])
    print("")
    print("without zeros")
    print("means ", np.mean(non_zero_AIS[i]), " ", np.mean(non_zero_AIS[i + 1]))
    print("stds ", np.std(non_zero_AIS[i]), np.std(non_zero_AIS[i + 1]))
    #print(len(TE[i]))
    print(scipy.stats.ttest_rel(np.array(non_zero_AIS[i]), np.array(non_zero_AIS[i + 1]), axis = None,
           alternative = 'less')[1])
    print("\n\n")


plt.boxplot(np.transpose(AIS[:, :]), showfliers = False)

#plt.hist(np.argmax(AIS, axis = 0))

#plt.scatter(np.arange(0, 60, 1), AIS[:, 3])
#plt.yscale("log")
#sns.heatmap(corrected_TE, vmin = 0, cmap = 'viridis')
plt.show()

#plt.savefig("figures_wagenaar/" + str(path)[16:-5] + "_" + str(path)[-4] + ".png")
#plt.clf()

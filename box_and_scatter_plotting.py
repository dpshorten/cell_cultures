import numpy as np
import h5py
import seaborn as sns
import os
import matplotlib.pyplot as plt
import matplotlib.colors as clr

import params
import utils

def plot_box_and_scatter(TE, sub_base_folder, run_group_index):

    fig, axs = utils.make_basic(False, run_group_index, special_axes_label_size = 16)

    #plt.subplots_adjust(hspace = 0.4, left = 0.2)

    for run_index_in_group in range(len(params.RUN_GROUPS[run_group_index])):
        run_index = params.RUN_GROUPS[run_group_index][run_index_in_group]

        print(run_index)

        this_runs_flattened_TE = [TE[run_index, i, :, :].flatten() for i in range(TE.shape[1])]
        #if run_index == 11:
        #    this_runs_flattened_TE = [this_runs_flattened_TE[1], this_runs_flattened_TE[2]]
        # Remove missing values denoted with -1
        this_runs_flattened_TE = [this_runs_flattened_TE[i][this_runs_flattened_TE[i] != -1]
                                  for i in range(len(this_runs_flattened_TE))]
        # Remove the resulting empty list for day 1
        this_runs_flattened_TE = [this_runs_flattened_TE[i]
                                  for i in range(len(this_runs_flattened_TE))
                                  if this_runs_flattened_TE[i].shape[0] > 0]

        # Remove outliers
        for i in range(len(this_runs_flattened_TE)):
            if len(this_runs_flattened_TE[i]) > 5:
                this_runs_flattened_TE[i] =  this_runs_flattened_TE[i][abs(this_runs_flattened_TE[i]
                                                                   - np.mean(this_runs_flattened_TE[i]))
                                                                   < 10 * np.std(this_runs_flattened_TE[i])]
        if run_index == 11:
            fig.delaxes(axs.flatten()[1])


        # output the means for the table
        print(params.RUNS[run_index], "means")
        for i in range(len(this_runs_flattened_TE)):
            if len(this_runs_flattened_TE[i]) > 1:
                print(np.mean(this_runs_flattened_TE[i]), end = ' ')
            else:
                print("-", end = ' ')
        print("")

        sns.boxplot(data = this_runs_flattened_TE[:], ax = axs[run_index_in_group], showfliers = False,
             palette = "colorblind", linewidth = 2)
        sns.stripplot(data = this_runs_flattened_TE[:], ax = axs[run_index_in_group], palette = "colorblind",
             linewidth = 1, size = 3, jitter = 0.3)

        axs[run_index_in_group].set_ylabel(r'TE (nats.s$^{-1}$)')
        axs[run_index_in_group].set_xticks(list(range(len(params.DAYS[run_index][:]))))
        axs[run_index_in_group].set_xticklabels(params.DAYS[run_index][:])

    if run_index == 11:
        #pass
        axs[len(params.RUN_GROUPS[run_group_index]) - 1].set_xlabel("development stage")
    else:
        axs[len(params.RUN_GROUPS[run_group_index]) - 1].set_xlabel("day")

    plt.savefig(params.BASE_FIGURE_FOLDER + sub_base_folder + params.DISTRIBUTIONS_FIGURE_FOLDER
                + params.RUN_GROUP_PREFIXES[run_group_index] + "box_and_scatter_with_zeros.pdf")

using DelimitedFiles

function read_spike_trains_from_csv(file_name)

    f = open(file_name)
    lines = readlines(f)
    spikes = []
    for line in lines
        temp = Float64[]
        times = split(line, ",")
        for time in times
            if length(time) > 0
                push!(temp, parse(Float64, time))
            end
        end
        #if (length(temp) > 0)
        #    temp = temp .- temp[1] .+ 1
        #end
        #temp = temp + (rand(length(temp)) .- 0.5)
        sort!(temp)
        push!(spikes, temp)
    end
    return spikes
end

function read_target_and_source_from_csv(file_name, target_index, source_index, max_index)

    target_spikes = Int64[]
    source_spikes = Int64[]
    f = open(file_name)
    for i = 1:max_index
        line = readline(f)
        if i == target_index || i == source_index
            temp = Float64[]
            times = split(line, ",")
            for time in times
                if length(time) > 0
                    push!(temp, parse(Float64, time))
                end
            end
            if i == target_index
                target_spikes = deepcopy(temp)
            else
                source_spikes = deepcopy(temp)
            end
        end
    end
    close(f)
    return target_spikes, source_spikes
end

function separate_locals_into_burst_status(bursts_prefix_filename, locals, surrogate_locals, event_times)

    stops = readdlm(string(bursts_prefix_filename, "stops"))
    stops = dropdims(stops, dims = 2)
    starts = readdlm(string(bursts_prefix_filename, "starts"))
    starts = dropdims(starts, dims = 2)

    event_index = 1
    next_stop = 1
    next_start = 1
    in_burst = false
    num_in_burst = 0
    total_TE_in_burst = 0
    surrogates_in_burst = zeros(size(surrogate_locals, 1))
    num_outside_burst = 0
    total_TE_outside_burst = 0
    surrogates_outside_burst = zeros(size(surrogate_locals, 1))
    while event_index <= length(event_times)
        if in_burst
            if next_stop >= length(stops) || event_times[event_index] < stops[next_stop]
                num_in_burst += 1
                total_TE_in_burst += locals[event_index]
                surrogates_in_burst .+= surrogate_locals[:, event_index]
                event_index += 1
            else
                in_burst = false
                next_stop += 1
            end
        else
            if next_start >= length(starts) || event_times[event_index] < starts[next_start]
                num_outside_burst += 1
                total_TE_outside_burst += locals[event_index]
                surrogates_outside_burst .+= surrogate_locals[:, event_index]
                event_index += 1
            else
                in_burst = true
                next_start += 1
            end
        end
    end
    #println("inside ", total_TE_in_burst, " ", num_in_burst)
    #println("outside ", total_TE_outside_burst, " ", num_outside_burst)
    if num_in_burst + num_outside_burst != length(event_times)
        println("burst nums mismatch")
    end
    return total_TE_in_burst, surrogates_in_burst, num_in_burst, total_TE_outside_burst, surrogates_outside_burst, num_outside_burst
end

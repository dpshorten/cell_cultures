import numpy as np
import h5py
import seaborn as sns
import os
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import pandas as pd
import scipy.stats

import params

OUTPUT_FILE_NAMES = [
    "fancy_edge_TE.pdf",
    "fancy_outgoing_TE.pdf",
    "fancy_outdegee.pdf",
    "fancy_incoming_TE.pdf",
    "fancy_indegee.pdf",
    "fancy_total_TE.pdf",
    "fancy_ratio.pdf"
]

def plot_fancy(TE, sub_base_folder, type):
    X = []
    Y = []
    sizes = []
    hues = []
    for run_index in range(len(params.RUNS)):

        this_runs_TE = TE[run_index, : , :, :]
        #this_runs_TE = this_
        this_runs_TE = np.ma.array(this_runs_TE, mask = this_runs_TE == -1)

        data = np.array([])
        if type == 1:
            data = np.reshape(this_runs_TE, (this_runs_TE.shape[0],  3600))
        elif type == 2:
            data = np.mean(this_runs_TE, axis = 2)
            #data = data[~data.mask]
        elif type == 3:
            data = np.count_nonzero(this_runs_TE, axis = 2)
        elif type == 4:
            data = np.mean(this_runs_TE, axis = 1)
        elif type == 5:
            data = np.count_nonzero(this_runs_TE, axis = 1)
        elif type == 6:
            data = np.mean(this_runs_TE, axis = 1) + np.mean(this_runs_TE, axis = 2)
        elif type == 7:
            out = np.mean(this_runs_TE, axis = 2)
            in_TE = np.mean(this_runs_TE, axis = 1)
            data = out/(out + in_TE + 0.001)

        for i in range(len(params.DAYS[run_index])):
            for j in range(i + 1, len(params.DAYS[run_index])):
                if not ((run_index, i) in [(0, 0), (1, 0)]):
                #if True:
                    x_dat = data[i, :]
                    y_dat = data[j, :]
                    combined_mask = x_dat.mask | y_dat.mask
                    x_dat = x_dat[~combined_mask]
                    y_dat = y_dat[~combined_mask]
                    #sizes.append(50 * (0.05 + np.corrcoef(data[i, :], data[j, :])[0, 1]))
                    if (len(x_dat) > 0) and np.count_nonzero(x_dat) > 0 and np.count_nonzero(y_dat) > 0:
                        sizes.append(1000)
                        hues.append(scipy.stats.spearmanr(x_dat, y_dat)[0])
                        X.append(int(params.DAYS[run_index][i]))
                        Y.append(int(params.DAYS[run_index][j]))

    kw = dict(prop="colors", num=5)
    plt.rc('legend', fontsize=16)
    scatter = plt.scatter(X, Y, s = 100, cmap = plt.cm.plasma, c = hues)
    #plt.legend()
    handles, labels = scatter.legend_elements(**kw)
    legend2 = plt.legend(handles, labels, loc="lower right", title=r'$\rho$', title_fontsize = 'large')
    # L=plt.legend()
    # L.get_texts()[0].set_text('Low\nCorrelation')
    # L.get_texts()[1].set_text('')
    # L.get_texts()[2].set_text('')
    # L.get_texts()[3].set_text('High\nCorrelation')
    plt.xlabel("Earlier Day")
    plt.ylabel("Later Day")
    plt.xlim((0, 1.1 * np.max(X)))
    plt.ylim((0, 1.1 * np.max(Y)))

    x_straight = np.linspace(0, 1.1 * np.max(X), 1000)
    y_straight = np.linspace(0, 1.1 * np.max(X), 1000)
    plt.plot(x_straight, y_straight, c = 'black', linestyle = '--')

    plt.savefig(params.BASE_FIGURE_FOLDER +  sub_base_folder + params.CORRELATIONS_OVER_TIME_FIGURE_FOLDER + OUTPUT_FILE_NAMES[type - 1], bbox_inches = 'tight')

    plt.clf()

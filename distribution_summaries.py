import numpy as np
from scipy import stats

import params

def summarise_distributions(TE):

    shapiro_p = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS))
    shapiro_p_no_zeros = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS))
    shapiro_p_straight_normal = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS))
    ks_no_zeros = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS))
    ks_statistic_no_zeros = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS))
    wasserstein_no_zeros = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS))
    energy_no_zeros = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS))
    num_links = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS), dtype = np.int32)
    means = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS))
    stds = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS))

    for run_index in range(params.NUM_RUNS):
        for day_index in range(params.MAX_NUM_DAYS):

            if day_index >= len(params.DAYS[run_index]):
                continue

            else:

                this_days_TE = TE[run_index, day_index, :, :].flatten()
                # Remove missing values designated by -1
                this_days_TE = this_days_TE[this_days_TE != -1]
                # Remove zeros
                this_days_TE_no_zeros = this_days_TE[this_days_TE != 0]

                shapiro_test = stats.shapiro(np.log(this_days_TE.flatten()))
                shapiro_p[run_index, day_index] = shapiro_test[1]
                if len(this_days_TE_no_zeros) > 2:

                    shapiro_test_no_zeros = stats.shapiro(np.log(this_days_TE_no_zeros.flatten()))
                    shapiro_p_no_zeros[run_index, day_index] = shapiro_test_no_zeros[1]
                    shapiro_p_straight_normal[run_index, day_index] = stats.shapiro(this_days_TE_no_zeros.flatten())[1]

                    generated_normal = np.random.normal(loc = np.mean(np.log(this_days_TE_no_zeros.flatten())),
                                                        scale = np.std(np.log(this_days_TE_no_zeros.flatten())),
                                                        size = (10 * len(this_days_TE_no_zeros.flatten()),))
                    wasserstein_no_zeros[run_index, day_index] = stats.wasserstein_distance(np.log(this_days_TE_no_zeros.flatten()),
                                                                      generated_normal)
                    ks_no_zeros[run_index, day_index] = stats.kstest(np.log(this_days_TE_no_zeros.flatten()), generated_normal)[1]
                    ks_statistic_no_zeros[run_index, day_index] = stats.kstest(np.log(this_days_TE_no_zeros.flatten()), generated_normal)[0]
                    energy_no_zeros[run_index, day_index] = stats.energy_distance(np.log(this_days_TE_no_zeros.flatten()),
                                                                      generated_normal)
                num_links[run_index, day_index] = len(this_days_TE_no_zeros)
                means[run_index, day_index] = np.mean(this_days_TE.flatten())
                stds[run_index, day_index] = np.std(this_days_TE.flatten())



    print("\n\n***************** Distribution summaries **************************\n")
    print("Mean with zeros\n")
    print(means)
    print("")
    print("Stds with zeros\n")
    print(stds)
    print("")
    print("Shapiro tests\n")
    print(shapiro_p)
    print("")
    print("Shapiro tests without zeros\n")
    print(shapiro_p_no_zeros)
    print("")
    print("Shapiro tests for straight normality\n")
    print(shapiro_p_straight_normal)
    print("")
    print("KS tests without zeros\n")
    print(ks_no_zeros)
    print("")
    print("KS statistic without zeros\n")
    print(ks_statistic_no_zeros)
    print("")
    print("Wasserstein distances from normal\n")
    print(wasserstein_no_zeros)
    print("")
    print("Energy distances from normal\n")
    print(energy_no_zeros)
    print("")
    print("Number of significant links\n")
    print(num_links)

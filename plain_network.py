import numpy as np
import h5py
import seaborn as sns
import os
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import networkx as nx
import statsmodels.api as sm
import pyspike as spk
import csv


from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import params

CULTURE_LABELS_TOP_GAP = 0.225
CULTURE_LABELS_GAP = 0.19

options = {
    #'node_color': 'black',
    #'node_size': 2,
    #'edge_color': 'black',
    #'width': 3.0,
    #'alpha': 0.5,
    'connectionstyle': "arc3,rad=0.1",
    'arrowstyle': '-|>',
    'arrowsize': 3,
    #'edge_cmap': plt.cm.Blues,
}

POS_DELTA = 0.1



def make_network(node_values, name):

    G = nx.DiGraph()

    labeldict = {}

    for i in range(60):
        if i != 14:
            position = np.argwhere(params.ELECTRODE_POSITIONS == i)[0]
            G.add_node(i, pos = [params.POS_DELTA * position[1], 1 - params.POS_DELTA * position[0]])
            labeldict[i] = str(i)

    node_sizes = 20 * np.ones(59)
            #node_sizes = np.delete(node_sizes, 14)

    node_sizes = list(node_sizes)

    nx.draw(G, nx.get_node_attributes(G, 'pos'), node_color = 'none',
            node_size = node_sizes, with_labels = True, labels = labeldict, font_size = 24,
            **options)

    plt.savefig(params.BASE_FIGURE_FOLDER + params.NORMAL_SUB_BASE_FIGURE_FOLDER + params.NETS_AND_MAPS_FIGURE_FOLDER + name + ".pdf")

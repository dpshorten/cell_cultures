import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import networkx as nx
import statsmodels.api as sm

import params

def plot_distance_TE_correlations(list_of_networks, TE, sub_base_folder, include_zeros = False):

    plt.rc('xtick', labelsize=16)
    plt.rc('ytick', labelsize=16)

    fig_dist, axs_dist = utils.make_basic(True, trim_first_column = True)
    fig_dist.subplots_adjust(hspace = 0.2, wspace = 0.2)

    fig_proportions, axs_proportions = utils.make_basic(True, trim_first_column = True)
    fig_proportions.subplots_adjust(hspace = 0.2, wspace = 0.2)

    if include_zeros:
        print("\n\n***** Distance-TE regression results with zeros*****\n")
    else:
        print("\n\n***** Distance-TE regression results without zeros*****\n")
    for run_index in range(params.NUM_RUNS):
        for day_index in range(1, params.MAX_NUM_DAYS):
            if day_index >= len(params.DAYS[run_index]):
                axs_dist[run_index, day_index - 1].axis('off')
                axs_proportions[run_index, day_index - 1].axis('off')

            else:
                corrected_TE = TE[run_index, day_index, :, :]
                proportions_dict = dict()

                distances = []
                TEs = []
                for i in range(corrected_TE.shape[0]):
                    for j in range(corrected_TE.shape[1]):

                        source = np.where(params.ELECTRODE_POSITIONS == i)
                        target = np.where(params.ELECTRODE_POSITIONS == j)
                        if i != 14 and j != 14 and i != j and (corrected_TE[i, j] != 0 or include_zeros):
                            distance = np.sqrt((source[0][0] - target[0][0])**2 + (source[1][0] - target[1][0])**2)
                            TEs.append(corrected_TE[i, j])
                            distances.append(distance)
                        if i != 14 and j != 14 and i != j and include_zeros:
                            distance = np.sqrt((source[0][0] - target[0][0])**2 + (source[1][0] - target[1][0])**2)
                            if distance in proportions_dict.keys():
                                if corrected_TE[i, j] != 0:
                                    proportions_dict[distance][1] += 1
                                else:
                                    proportions_dict[distance][0] += 1
                            else:
                                if corrected_TE[i, j] != 0:
                                    proportions_dict[distance] = [0, 1]
                                else:
                                    proportions_dict[distance] = [1, 0]

                if len(distances) >= 1:

                    if include_zeros:
                        proportions = []
                        distances_for_proportions = []
                        for distance in proportions_dict:
                            proportions.append(proportions_dict[distance][1]/(proportions_dict[distance][1] + proportions_dict[distance][0]))
                            distances_for_proportions.append(distance)

                        sns.regplot(distances_for_proportions, proportions, ax = axs_proportions[run_index, day_index - 1],
                                line_kws={'linewidth':1}, scatter_kws={'s':1}, x_jitter = 0.1,  color = sns.color_palette(palette = 'colorblind')[0])

                    sns.regplot(distances, TEs, ax = axs_dist[run_index, day_index - 1], line_kws={'linewidth':1}, scatter_kws={'s':1}, x_jitter = 0.1,
                                 color = sns.color_palette(palette = 'colorblind')[0])
                    #axs_dist[run_index, day_index].set_xticks([])
                    #axs_dist[run_index, day_index].set_yticks([])
                    distances2 = sm.add_constant(distances)
                    model = sm.OLS(TEs, distances2).fit()
                    #print(model.summary())
                    print(params.RUNS[run_index], " ", params.DAYS[run_index][day_index], " ", model.params[1], " ", model.pvalues[1])
                else:
                    axs_dist[run_index, day_index - 1].axis('off')
                    axs_proportions[run_index, day_index - 1].axis('off')
    print("\n\n")
    if include_zeros:
        fig_dist.savefig(params.BASE_FIGURE_FOLDER + sub_base_folder + params.STATIC_CORRELATIONS_BY_EDGE_FIGURE_FOLDER +  "distance-TE_with_zeros.pdf")
        fig_proportions.savefig(params.BASE_FIGURE_FOLDER + sub_base_folder + params.STATIC_CORRELATIONS_BY_EDGE_FIGURE_FOLDER +  "distance-proportions.pdf")
    else:
        fig_dist.savefig(params.BASE_FIGURE_FOLDER + sub_base_folder + params.STATIC_CORRELATIONS_BY_EDGE_FIGURE_FOLDER +  "distance-TE_without_zeros.pdf")

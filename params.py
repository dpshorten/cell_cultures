import numpy as np

RUNS = [
    "1-1",
    "1-2",
    "1-3",
    "1-4",
    "1-5",
    "2-1",
    "2-2",
    "2-3",
    "2-4",
    "2-5",
    "2-6",
    "only_stdp",
]

RUN_GROUPS = [
    [0, 2, 6, 9],
    [1, 3, 4, 5, 7, 8, 10],
    [11],
]

# Each tuple is (run_index, day_index)
EXCLUDE_FOR_BURST_ANALYSIS = [
    #(4, 2),
    (7, 1),
    (10, 1),
]

RUN_GROUP_PREFIXES = [
    "main_",
    "appendix_",
    "stdp_",
]

BASE_SPIKE_TRAIN_FOLDER = "extracted_data_wagenaar/"
BURST_RECORDS_FOLDER = "burst_records_artemis/"
ORIGINAL_SPIKE_TRAIN_FILES = [
    ["1-1-4.2.spk", "1-1-14.2.spk", "1-1-20.2.spk"],
    ["1-2-6.2.spk", "1-2-11.2.spk","1-2-17.2.spk"],
    ["1-3-5.2.spk", "1-3-10.2.spk", "1-3-16.2.spk", "1-3-24.2.spk"],
    ["1-4-8.2.spk", "1-4-13.2.spk","1-4-19.2.spk"],
    ["1-5-7.2.spk", "1-5-12.2.spk", "1-5-18.2.spk"],
    ["2-1-14.2.spk", "2-1-32.2.spk"],
    ["2-2-9.2.spk", "2-2-15.2.spk", "2-2-21.2.spk", "2-2-33.2.spk"],
    #["2-2-15.2.spk", "2-2-21.2.spk", "2-2-33.2.spk"],
    ["2-3-6.2.spk", "2-3-12.2.spk", "2-3-24.2.spk"],
    ["2-4-3.1.spk", "2-4-5.1.spk", "2-4-11.1.spk"],
    ["2-5-4.1.spk", "2-5-10.1.spk", "2-5-22.2.spk", "2-5-28.1.spk"],
    #["2-5-10.1.spk", "2-5-22.2.spk", "2-5-28.1.spk"],
    ["2-6-7.1.spk", "2-6-13.1.spk", "2-6-31.1.spk"],
    ["stdp_4_early.spk", "stdp_4_mid.spk", "stdp_4_late.spk"],
    #["stdp_4_mid.spk", "stdp_4_late.spk"],
#    ["stdp_4_late.spk"],
]

FILES = [
    ["resub_1-1-4.2-*", "resub_1-1-14.2-*", "resub_1-1-20.2-*"],
    ["resub_1-2-6.2-*", "resub_1-2-11.2-*", "resub_1-2-17.2-*"],
    ["resub_1-3-5.2-*", "resub_1-3-10.2-*", "resub_1-3-16.2-*", "resub_1-3-24.2-*"],
    ["resub_1-4-8.2-*", "resub_1-4-13.2-*", "resub_1-4-19.2-*"],
    ["resub_1-5-7.2-*", "resub_1-5-12.2-*", "resub_1-5-18.2-*"],
    ["resub_2-1-14.2-*", "resub_2-1-32.2-*"],
    ["resub_2-2-9.2-*", "resub_2-2-15.2-*", "resub_2-2-21.2-*", "resub_2-2-33.2-*"],
    ["resub_2-3-6.2-*", "resub_2-3-12.2-*", "resub_2-3-24.2-*"],
    ["condo_resub_2-4-3.1-*", "condo_resub_2-4-5.1-*", "condo_resub_2-4-11.1-*"],
    ["resub_2-5-4.1-*", "resub_2-5-10.1-*", "resub_2-5-22.2-*", "resub_2-5-28.1-*"],
    ["resub_2-6-7.1-*", "resub_2-6-13.1-*", "resub_2-6-31.1-*"],
    ["new_burst_pos9_stdp_4_early-*", "new_burst_pos9_stdp_4_mid-*", "new_burst_pos9_stdp_4_late-*"],
    #["new_burst_pos_stdp_4_mid-*", "new_burst_pos4_stdp_4_late-*"],
]

DAYS = [
    [4, 14, 20],
    [6, 11, 17],
    [5, 10, 16, 24],
    [8, 13, 19],
    [7, 12, 18],
    [14, 32],
    [9, 15, 21, 33],
    #[15, 21, 33],
    [6, 12, 24],
    [3, 5, 11],
    [4, 10, 22, 28],
    #[10, 22, 28],
    [7, 13, 31],
    ["early", "mid", "late"],
    #["mid", "late"],
]

UPPER_ELECTRODE_INDEX = 59

DEAD_ELECTRODES = [14, 37, 43]

ELECTRODE_POSITIONS = np.array(
    [
        [-1, 23, 25,    28,	31,	34,	36, -1],
        [20, 21, 24,	29,	30,	35,	38,	39],
        [18, 19, 22,	27,	32,	37,	40,	41],
        [15, 16, 17,	26,	33,	42,	43,	44],
        [-1, 13, 12,	3,	56,	47,	46,	45],
        [11, 10,  7,	2,	57,	52,	49,	48],
        [9,  8,   5,	0,	59,	54,	51,	50],
        [-1, 6,   4,	1,	58,	55,	53, -1]
    ]
)

POS_DELTA = 0.1

BURST_DEFINITION_INTERVAL_PROP_OF_MEAN_INTERVAL = 0.5
BURST_DEFINITION_INTERVAL_PROP_OF_MEAN_INTERVAL_IZHI = 0.5

NUM_RUNS = len(RUNS)
#MAX_NUM_DAYS = max([len(run_days) for run_days in DAYS])
MAX_NUM_DAYS = 4
MAX_NUM_DAYS_BY_GROUP = [4, 3, 3]
BURST_PLOT_KILLED_COLS = [0, 0, 0]

PICKLED_DATA_FILE_NAME = "pickled_exp_results.pkl"

SPIKE_TRAINS_FOLDER = "extracted_data_wagenaar/"
#DATA_FOLDER = "results_resub/"
DATA_FOLDER = "results_wagenaar/"
#DATA_FOLDER = "results_full/"

BASE_FIGURE_FOLDER = "figures_wagenaar/"
BURSTS_SUB_BASE_FIGURE_FOLDER = "burst_TE/"
NONBURSTS_SUB_BASE_FIGURE_FOLDER = "outside_burst_TE/"
RATE_NORMALISED_SUB_BASE_FIGURE_FOLDER = "rate_normalised_TE/"
NORMAL_SUB_BASE_FIGURE_FOLDER = "normal_TE/"
DISTRIBUTIONS_FIGURE_FOLDER = "distributions/"
CORRELATIONS_OVER_TIME_FIGURE_FOLDER = "correlations_over_time/"
STATIC_CORRELATIONS_BY_NODE_FIGURE_FOLDER = "static_correlations/by_node/"
STATIC_CORRELATIONS_BY_EDGE_FIGURE_FOLDER = "static_correlations/by_edge/"
NETS_AND_MAPS_FIGURE_FOLDER = "nets_and_maps/"


P_CUTOFF = 2.9e-6
#P_CUTOFF = 2.9e-1
NUM_SURROGATES = 100
NUM_NODES = 60


FIG_WIDTH = 12
FIG_HEIGHTS = [12, 20, 4]
CULTURE_LABELS_TOP_GAPS = [0.205, 0.172, 0.2]
CULTURE_LABELS_GAPS = [0.205, 0.114, 0.2]
CULTURE_LABELS_SIZES = [14, 14, 14]
H_SPACES = [0.35, 0.35, 0.35]
W_SPACES = [0.25, 0.25, 0.25]
TICK_SIZES = [12, 12, 12]
LABEL_SIZES = [12, 12, 12]
TITLE_SIZES = [12, 12, 12]


NETWORK_LEGEND_POSITIONS = [(0, 3), (3, 2), (0, 0)]

#BONFERRONI_NUM_BURSTS =

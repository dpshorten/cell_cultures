import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np


data = pd.read_csv("out2.csv")
data = data[data["p"] >= 90]
minimum = min(data["value"])
data["value"] = data["value"] + minimum

g = nx.from_pandas_edgelist(data, "source", "target", edge_attr = "value")

foo = []
for i in range(60):
    g.add_node(float(i))
    foo.append(float(i))
foo.append(61.0)
foo = np.array(foo)

    
pos = nx.drawing.layout.circular_layout(g)
options = {
     'width': 0,
     'alpha': 0.7,
}

edge_color = []
for edge in g.edges():
    edge_color.append(edge[1])

edge_width = []
for edge in g.edges(data = 'value'):
    #edge_width.append(7e3 * edge[2])
    edge_width.append(1e4 * edge[2])
    
nx.draw_networkx_nodes(g, pos = pos, with_labels = True, node_color = foo, cmap = plt.cm.viridis, alpha = 0.6)
nx.draw_networkx_edges(g, pos = pos, width = edge_width,
                       edge_color = edge_color, edge_cmap = plt.cm.viridis, alpha = 0.6)
#for edge in g.edges(data='value'):
 #   print(edge[0])
  #  nx.draw_networkx_edges(g, pos, edgelist=[edge], width=edge[2]*1e4, alpha = 0.5, edge_color = [int(edge[0])],
   #                        edge_cmap = plt.cm.viridis)
plt.show()


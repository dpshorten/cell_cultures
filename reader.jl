#NUM_TO_READ = Int(1e5)
FOLDER = "raw_data_wagenaar/2-6/"

struct SpikeInfo
    time::UInt64
    channel::Int16
    height::Int16
    width::Int16
    context::Array{Int16, 1}
    threshold::Int16
end



for filename in readdir(FOLDER)
    println(filename)
    if occursin("spike", filename)
        f = open(string(FOLDER, filename), "r")
    else
        continue
    end

    spikey_spikes = []

    for i = 1:60
        push!(spikey_spikes, [])
    end
    while !eof(f)
        time = read(f, UInt64)
        channel = read(f, Int16)
        append!(spikey_spikes[channel + 1], Int64(time))
        height = read(f, Int16)
        width = read(f, Int16)
        for j = 1:74
            context = read(f, Int16)
        end
        threshold = read(f, Int16)
    end

    out = open(string(FOLDER, chop(filename, tail = 5), "spk"), "w")
    for spikes in spikey_spikes
        println(length(spikes))
        for spike in spikes
            write(out, string(spike), ",")
        end
        write(out, "\n")
    end
    println()
    close(out)
end

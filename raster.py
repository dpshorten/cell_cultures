import numpy as np
import matplotlib.pyplot as plt
from csv import reader

def read_spikes(reader):
    spikes = []
    #i = 0
    for line in reader:
        if len(line) > 0:
            start = float(line[0])
        else:
            start = 0
        line = [float(time) for time in line if len(time) > 0 and float(time) < (start + 1 * 60 * 60 * 2.5e4)]
        #i += 1
        #line = line[:5000]
        spikes.append(line)
    return spikes

def read_pop(reader):
    spikes = []
    for line in reader:
        line = float(line[0])
        spikes.append(line)
    return spikes

#base_file_pattern = "2-2/2-2-33.2"
#base_file_pattern = "2-5/2-5-28.1"
#base_file_pattern = "1-1/1-1-20.2"
#base_file_pattern = "1-3/1-3-24.2"
#base_file_pattern = "1-5/1-5-18.2"
#base_file_pattern = "1-5/1-5-12.2"
#base_file_pattern = "2-6/2-6-31.1"
#base_file_pattern = "2-4/2-4-5.1"
#base_file_pattern = "only_stdp/stdp_4_mid"
base_file_pattern = "only_stdp/stdp_4_late"
f1 = open('extracted_data_wagenaar/' + base_file_pattern + '.spk', 'r')
f_starts = open('burst_records3/' + base_file_pattern + '.spk.starts', 'r')
f_stops = open('burst_records3/' + base_file_pattern + '.spk.stops', 'r')
f_pop_starts = open('burst_records3/' + base_file_pattern + '.spk.pop_starts', 'r')
f_pop_stops = open('burst_records3/' + base_file_pattern + '.spk.pop_stops', 'r')

csv_reader1 = reader(f1,  delimiter=',')
csv_reader_starts = reader(f_starts,  delimiter=',')
csv_reader_stops = reader(f_stops,  delimiter=',')
csv_reader_pop_starts = reader(f_pop_starts,  delimiter=',')
csv_reader_pop_stops = reader(f_pop_stops,  delimiter=',')

spikes1 = read_spikes(csv_reader1)

all_intervals = []
for i in range(len(spikes1)):
    foo = np.array(spikes1[i])
    #print(len(foo))
    if len(foo) > 2:
        #all_intervals.append(np.mean(foo[1:] - foo[:-1]))
        all_intervals.extend(foo[1:] - foo[:-1])
#intervals = np.sort(intervals)
print("mean ", np.mean(all_intervals))
print("quantiles 0.01, 0.05, 0.25, 0.5, 0.95, 0.99 ", np.quantile(all_intervals, [0.01, 0.05, 0.25, 0.5, 0.95, 0.99]))
#print(intervals[:100])
#print(np.mean(intervals))
#print(np.median(intervals))
#print("60th ", np.quantile(intervals, 0.6))
#print("75th ", np.quantile(intervals, 0.75))
#print("80th ", np.quantile(intervals, 0.8))
#plt.hist(intervals, range = (0, 1000), bins = 10)
#plt.show()

starts = read_spikes(csv_reader_starts)
stops = read_spikes(csv_reader_stops)
pop_starts = read_pop(csv_reader_pop_starts)
pop_stops = read_pop(csv_reader_pop_stops)

# for spikeys in spikes1:
#     if len(spikeys) > 0:
#         length = 4e-5 * (spikeys[-1] - spikeys[0])
#         rate = len(spikeys) / length
#         print(rate)

#lineoffsets = [0.6, 0.4, 0.2, 0.0]
lineoffsets = [8, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2]
colors = ['blue', 'green', 'red', 'blue', 'green', 'red', 'blue', 'green', 'red', 'green', 'red']
#colors = ['blue', 'green', 'red', 'black']
#plt.eventplot([spikes1[8], starts[8], stops[8], spikes1[11], starts[11], stops[11], spikes1[43], starts[43], stops[43], pop_starts, pop_stops], linewidth = 0.5, lineoffsets = lineoffsets, color = colors, linelength = 0.9)
plt.eventplot([spikes1[19], starts[19], stops[19], spikes1[18], starts[18], stops[18], spikes1[11], starts[11], stops[11], pop_starts, pop_stops], linewidth = 0.5, lineoffsets = lineoffsets, color = colors, linelength = 0.9)
#plt.eventplot([spikes1[5], starts[5], stops[5], spikes1[21], starts[21], stops[21], spikes1[48], starts[48], stops[48], pop_starts, pop_stops], linewidth = 0.5, lineoffsets = lineoffsets, color = colors, linelength = 0.9)

#plt.eventplot([spikes1[15],spikes1[16], spikes1[18],spikes1[19]], color = colors, linestyles = "-.", linewidth = 0.75, lineoffsets = lineoffsets, linelength = 1.05)
#plt.eventplot(spikes1, linewidth = 0.5)
#plt.eventplot(starts, linewidth = 0.5)
plt.show()

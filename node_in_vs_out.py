import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.colors as clr
from matplotlib.offsetbox import AnchoredOffsetbox, TextArea, HPacker, VPacker
#from matplotlib import transforms
from matplotlib import transforms
import networkx as nx
import statsmodels.api as sm
import scipy.stats
from matplotlib.ticker import FormatStrFormatter
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import params
import utils

MARKER_SIZE = 3
COLOUR_MARKERS = sns.color_palette(palette = 'colorblind')[0]
COLOUR_LINES = sns.color_palette(palette = 'colorblind')[1]
MARKER_LINE_WIDTH = 4

def make_a_plot(x, y, run_group_index, name, sub_base_folder, xlabel, ylabel, colors = []):

    plt.rc('axes', labelsize = params.LABEL_SIZES[run_group_index])
    plt.rc('font', size = params.LABEL_SIZES[run_group_index])
    plt.rc('xtick', labelsize = params.TICK_SIZES[run_group_index])
    plt.rc('ytick', labelsize = params.TICK_SIZES[run_group_index])

    fig, axs = utils.make_basic(True, run_group_index, is_burst_plot = True)

    for run_index_in_group in range(len(x)):

        day_prefix = "day "
        if run_group_index == 2:
            day_prefix = ""

        run_index = params.RUN_GROUPS[run_group_index][run_index_in_group]
        day_grid_offset = params.MAX_NUM_DAYS_BY_GROUP[run_group_index] - len(params.DAYS[run_index]) - params.BURST_PLOT_KILLED_COLS[run_group_index]
        #day_index_start = - params.MAX_NUM_DAYS_BY_GROUP[run_group_index] + len(params.DAYS[run_index]) + params.BURST_PLOT_KILLED_COLS[run_group_index]
        for i in range(day_grid_offset):
            axs[run_index_in_group, i].axis('off')

        for day_index in range(params.MAX_NUM_DAYS_BY_GROUP[run_group_index] - params.BURST_PLOT_KILLED_COLS[run_group_index]):
            print(run_index, day_grid_offset, day_index)
            if day_index >= len(params.DAYS[run_index]):
                continue

            if run_group_index == 2:
                print(x[run_index_in_group][:5, :5])
                print(y[run_index_in_group][:5, :5])

            axs[run_index_in_group][day_index + day_grid_offset].set_xlabel(day_prefix + str(params.DAYS[run_index][day_index]))

            if (  np.count_nonzero(x[run_index_in_group][day_index, :]) < 5
                  or np.count_nonzero(y[run_index_in_group][day_index, :]) < 5
                  or ((run_index, day_index) in params.EXCLUDE_FOR_BURST_ANALYSIS)
                ):
                axs[run_index_in_group, day_index + day_grid_offset].axis('off')
                if run_group_index != 2:
                    axs[run_index_in_group, day_index + day_grid_offset].text(0.37, -0.28, day_prefix + str(params.DAYS[run_index][day_index]))
            else:

                this_x = x[run_index_in_group][day_index, :]
                print(this_x[1:5])
                this_y = y[run_index_in_group][day_index, :]

                combined_mask = this_x.mask | this_y.mask
                this_x = this_x[~combined_mask]
                this_y = this_y[~combined_mask]

                if len(this_x) == 0:
                    print("continue ", run_index, day_index)
                    continue

                #axs[run_index_in_group, day_index + day_grid_offset].yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
                #axs[run_index_in_group, day_index + day_grid_offset].xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
                range_x = np.max(this_x) - np.min(this_x)
                range_y = np.max(this_y) - np.min(this_y)
                axs[run_index_in_group, day_index + day_grid_offset].set_xlim([np.min(this_x) - 0.05 * range_x, np.max(this_x) + 0.05 * range_x])
                axs[run_index_in_group, day_index + day_grid_offset].set_ylim([np.min(this_y) - 0.05 * range_y, np.max(this_y) + 0.05 * range_y])

                if len(colors) == 0:
                    axs[run_index_in_group, day_index + day_grid_offset].scatter(this_x, this_y, s = MARKER_SIZE, color = COLOUR_MARKERS)
                    coef = np.polyfit(this_x, this_y, 1)
                    line_fn = np.poly1d(coef)
                    axs[run_index_in_group, day_index + day_grid_offset].plot(this_x, line_fn(this_x), color = COLOUR_LINES)
                    rho, p_rho = scipy.stats.spearmanr(this_x, this_y)
                    sig_string = ""
                    sig_string_bonf = ""
                    if p_rho < 0.01:
                        sig_string = "**"
                    elif p_rho < 0.05:
                        sig_string = "*"
                    if p_rho < 0.01/25:
                        sig_string_bonf = "**"
                    elif p_rho < 0.05/25:
                        sig_string_bonf = "*"

                    # rend = axs[run_index_in_group, day_index + day_grid_offset].figure.canvas.get_renderer()
                    # text = axs[run_index_in_group, day_index + day_grid_offset].text(np.min(this_x) + 0.1 * range_x, np.min(this_y) + 0.9 * range_y,
                    #          r'$\rho =' + "{:.2f}".format(rho) + r'$' + sig_string,
                    #          fontdict = {'size' : 12})
                    # text.draw(rend)
                    # ex = text.get_window_extent()
                    # t = transforms.offset_copy(text.get_transform(), x = ex.width, units = 'dots')
                    # text = axs[run_index_in_group, day_index + day_grid_offset].text(np.min(this_x) - 0.05 * range_x, np.min(this_y) + 0.9 * range_y,
                    #          sig_string_bonf, color = 'red',
                    #          fontdict = {'size' : 12}, transform = t)
                    # text.draw(rend)
                    # axs[run_index, day_index].text(np.min(this_x) + 0.1 * range_x, np.min(this_y) + 0.8 * range_y,
                    #          r'$p =' + "{:.2f}".format(p_rho) + r'$', fontdict = {'size' : 10})

                    rho_box_1 = TextArea(r'$\rho =' + "{:.2f}".format(rho) + r'$' + sig_string, textprops=dict(color="k", size=12, ha='left', va='top'))
                    rho_box_2 = TextArea(sig_string_bonf, textprops=dict(color="r", size=12, ha='left', va='top'))

                    ybox = HPacker(children=[rho_box_1, rho_box_2], align="top", pad=0, sep=0.25)

                    anchored_ybox = AnchoredOffsetbox(loc = 'upper center', child = ybox, pad = 0, bbox_to_anchor = (0.5, 1.05),
                                      bbox_transform=axs[run_index_in_group, day_index + day_grid_offset].transAxes, frameon = False)

                    axs[run_index_in_group, day_index + day_grid_offset].add_artist(anchored_ybox)

                else:
                    these_colors = colors[run_index_in_group][day_index, :]
                    these_colors = these_colors[~combined_mask]
                    axs[run_index_in_group, day_index + day_grid_offset].scatter(this_x, this_y, c = these_colors, cmap = 'plasma')

    if run_group_index == 2:
        for i in [3, 4, 5]:
            fig.delaxes(axs.flatten()[i])
        #axs[0][1].set_xlabel(str(params.DAYS[run_index][day_index]))
        #axs[0][2].set_xlabel(str(params.DAYS[run_index][day_index]))

    axs[0, 1].set_xlabel(xlabel + ", " + day_prefix + str(params.DAYS[params.RUN_GROUPS[run_group_index][0]][1]))
    axs[0, 1].set_ylabel(ylabel)

    if len(colors) != 0:
        sm = plt.cm.ScalarMappable(cmap = 'plasma', norm=plt.Normalize(vmin=0, vmax=1))
        sm.set_array([])
        axins = inset_axes(axs[0, 0],
                   width="20%",  # width = 5% of parent_bbox width
                   height="100%",  # height : 50%
                   loc='lower left',
                   bbox_to_anchor=(0.2, 0, 0.5, 1),
                   bbox_transform=axs[0, 0].transAxes,
                   borderpad=0.0,
                   )
        cbar = fig.colorbar(sm, cax = axins, ticks = [0, 1])
        cbar.ax.set_yticklabels(['early burster', 'late burster'])


    plt.savefig(params.BASE_FIGURE_FOLDER + sub_base_folder + params.STATIC_CORRELATIONS_BY_NODE_FIGURE_FOLDER + name + ".pdf")

def plot_in_vs_out_correlations(TE, mean_burst_positions, run_group_index, sub_base_folder):

    meaned_in = []
    meaned_out = []
    this_groups_mean_burst_positions = []

    for run_index in params.RUN_GROUPS[run_group_index]:
        this_runs_TE = TE[run_index, : , :, :]

        this_runs_TE = np.ma.array(this_runs_TE, mask = this_runs_TE == -1)
        this_runs_TE[this_runs_TE == -1] = 0
        meaned_in.append(np.mean(this_runs_TE, axis = 1))
        meaned_out.append(np.mean(this_runs_TE, axis = 2))
        this_groups_mean_burst_positions.append(mean_burst_positions[run_index])



    make_a_plot(this_groups_mean_burst_positions, meaned_in, run_group_index,
                params.RUN_GROUP_PREFIXES[run_group_index] +
                "burst_position_vs_normalised_TE_in", sub_base_folder,
                "mean burst position", r'mean in TE (nats per spike)')
    make_a_plot(this_groups_mean_burst_positions, meaned_out, run_group_index,
                params.RUN_GROUP_PREFIXES[run_group_index] +
                "burst_position_vs_normalised_TE_out", sub_base_folder,
                "mean burst position", r'mean out TE (nats per spike)')

    make_a_plot(meaned_out, meaned_in, run_group_index,
                params.RUN_GROUP_PREFIXES[run_group_index] +
                "TE_out_vs_TE_in_vs_burst_pos", sub_base_folder,
                r'mean out TE (nats per spike)',
                r'mean in TE (nats per spike)',
                colors = this_groups_mean_burst_positions)

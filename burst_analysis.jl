include("jl_spike_train_utils.jl")
using PyCall
using DelimitedFiles
using Statistics

py"""
import sys
sys.path.insert(0, "/home/david/cell_cultures/")
"""
@pyimport params
#params = pyimport("params.py")

#for i = 1:length(params.RUNS)
for i = 12:12
    for file_name in params.ORIGINAL_SPIKE_TRAIN_FILES[i]
        println(params.ORIGINAL_SPIKE_TRAIN_FILES[i])
        println(file_name)
        spikes = read_spike_trains_from_csv(string(
            params.BASE_SPIKE_TRAIN_FOLDER,
            params.RUNS[i],
            "/",
            file_name,
        ))

        for  j = 1:length(spikes)
            spikes[j] = spikes[j] + 1e-5 .* rand(length(spikes[j]))
        end

        all_burst_starts = []
        all_burst_stops = []

        k = -1
        IZHI_INDEX = 12
        for train in spikes
            if length(train) > 1
                rate_in_cycles = length(train)/(train[end] - train[1])
                if i == IZHI_INDEX
                    println("high int")
                    burst_def_interval = params.BURST_DEFINITION_INTERVAL_PROP_OF_MEAN_INTERVAL_IZHI * (1/rate_in_cycles)
                else
                    burst_def_interval = params.BURST_DEFINITION_INTERVAL_PROP_OF_MEAN_INTERVAL * (1/rate_in_cycles)
                end
            end
            burst_starts = []
            burst_stops = []
            in_burst = false

            #k += 1
            #if k == 14
            if size(train, 1) > 1
                push!(burst_starts, train[1])
                push!(burst_stops, train[2])
            else
                push!(burst_starts, 10)
                push!(burst_stops, 11)
            end

            #end

            for j = 3:size(train, 1)
                if !in_burst
                    int = 2
                    if i == IZHI_INDEX
                        int = 1
                    end
                    if train[j] - train[j-int] <= burst_def_interval
                        if i == IZHI_INDEX
                            push!(burst_starts, train[j-int])
                        else
                            push!(burst_starts, train[j-int])
                        end
                        in_burst = true
                    end
                else
                    #mult = 3
                    mult = 2
                    if i == IZHI_INDEX
                        mult = 2
                    end
                    if train[j] - train[j-1] > mult * burst_def_interval
                        if i == IZHI_INDEX
                            push!(burst_stops, train[j-1])
                        else
                            push!(burst_stops, train[j-1])
                        end
                        in_burst = false
                    end
                end
            end
            push!(all_burst_starts, burst_starts)
            push!(all_burst_stops, burst_stops)
        end

        writedlm(
            string(params.BURST_RECORDS_FOLDER, params.RUNS[i], "/", file_name, ".starts"),
            all_burst_starts,
            ',',
        )
        writedlm(
            string(params.BURST_RECORDS_FOLDER, params.RUNS[i], "/", file_name, ".stops"),
            all_burst_stops,
            ',',
        )

        electrodes_in_burst = falses(params.UPPER_ELECTRODE_INDEX + 1)
        electrodes_last_was_start = falses(params.UPPER_ELECTRODE_INDEX + 1)
        electrodes_next_start_index = ones(Int32, params.UPPER_ELECTRODE_INDEX + 1)
        electrodes_next_stop_index = ones(Int32, params.UPPER_ELECTRODE_INDEX + 1)
        #burst_during_present_burst = falses(params.UPPER_ELECTRODE_INDEX + 1)
        indices_of_bursters = []
        starts_of_bursters = []
        population_in_burst = false
        population_starts = []
        population_stops = []
        burst_start_places = []
        burst_start_times = []
        burst_start_precedences = []
        for j = 1:(params.UPPER_ELECTRODE_INDEX + 1)
            push!(burst_start_places, [])
            push!(burst_start_times, [])
            temp = []
            for j = 1:(params.UPPER_ELECTRODE_INDEX + 1)
                push!(temp, [])
            end
            push!(burst_start_precedences, temp)
        end

        not_at_end = true
        while not_at_end
            next_event_electrode = 0
            #next_event_timestamp = 1e30
            next_event_timestamp = 1e5

            num_still_going = 0
            for j = 1:length(electrodes_last_was_start)
                if (electrodes_next_start_index[j] > length(all_burst_starts[j])) & (electrodes_next_stop_index[j] > length(all_burst_stops[j]))
                    continue
                else
                    num_still_going += 1
                end

                if electrodes_last_was_start[j]
                    #println("stop", all_burst_stops[j][electrodes_next_stop_index[j]])
                    if all_burst_stops[j][electrodes_next_stop_index[j]] < next_event_timestamp
                        next_event_electrode = j
                        next_event_timestamp = all_burst_stops[j][electrodes_next_stop_index[j]]
                    end
                else
                    #println("start", all_burst_starts[j][electrodes_next_start_index[j]])
                    if all_burst_starts[j][electrodes_next_start_index[j]] < next_event_timestamp
                        next_event_electrode = j
                        next_event_timestamp = all_burst_starts[j][electrodes_next_start_index[j]]
                    end
                end
            end

            #println(num_still_going, " ", next_event_timestamp, " ", next_event_electrode)
            if num_still_going == 1
                not_at_end = false
            end

            if electrodes_last_was_start[next_event_electrode]
                electrodes_next_stop_index[next_event_electrode] += 1
                electrodes_last_was_start[next_event_electrode] = false
                electrodes_in_burst[next_event_electrode] = false
            else
                electrodes_next_start_index[next_event_electrode] += 1
                electrodes_last_was_start[next_event_electrode] = true
                electrodes_in_burst[next_event_electrode] = true
            end

            if population_in_burst
                for j = 1:length(electrodes_in_burst)
                    if electrodes_in_burst[j]
                        #burst_during_present_burst[j] = true

                        if !(j in indices_of_bursters)
                            push!(indices_of_bursters, j)
                            push!(starts_of_bursters, all_burst_starts[j][electrodes_next_start_index[j] - 1])
                        end
                    end
                end
            end

            upper = 15
            #upper = 12
            lower = 10
            #lower = 12
            if i == IZHI_INDEX
                upper = 2
                lower = 1
            end

            #println(next_event_electrode, " ", sum(electrodes_in_burst))
            #println()

            if (!population_in_burst) & (sum(electrodes_in_burst) >= upper)
                population_in_burst = true

                indices_of_bursters = []
                starts_of_bursters = []

                for j = 1:length(electrodes_in_burst)
                    if electrodes_in_burst[j]
                        push!(indices_of_bursters, j)
                        push!(starts_of_bursters, all_burst_starts[j][electrodes_next_start_index[j] - 1])
                    end
                end

                #println("out of burst ", electrodes_in_burst)


                if electrodes_last_was_start[next_event_electrode]
                    push!(
                        population_starts,
                        all_burst_starts[next_event_electrode][electrodes_next_start_index[next_event_electrode]-1],
                    )
                else
                    println("This does not make sense")
                    quit()
                end
            elseif (population_in_burst) & (sum(electrodes_in_burst) < lower)
                #println("in burst ", electrodes_in_burst)
                population_in_burst = false
                if !electrodes_last_was_start[next_event_electrode]
                    push!(
                        population_stops,
                        all_burst_stops[next_event_electrode][electrodes_next_start_index[next_event_electrode]-1],
                    )
                else
                    println("This does not make sense 2")
                    quit()
                end



                sort_order = sortperm(starts_of_bursters)
                for j = 1:length(sort_order)
                    push!(burst_start_places[indices_of_bursters[sort_order[j]]], j)
                    push!(burst_start_times[indices_of_bursters[sort_order[j]]],
                          starts_of_bursters[sort_order[j]] - minimum(starts_of_bursters))
                    for k = (j + 1):length(sort_order)
                        push!(burst_start_precedences[indices_of_bursters[sort_order[j]]][indices_of_bursters[sort_order[k]]], 0)
                        push!(burst_start_precedences[indices_of_bursters[sort_order[k]]][indices_of_bursters[sort_order[j]]], 1)
                    end
                end
                #burst_during_present_burst = falses(params.UPPER_ELECTRODE_INDEX + 1)
            end

        end

        writedlm(
            string(params.BURST_RECORDS_FOLDER, params.RUNS[i], "/", file_name, ".pop_starts"),
            population_starts,
            ',',
        )
        writedlm(
            string(params.BURST_RECORDS_FOLDER, params.RUNS[i], "/", file_name, ".pop_stops"),
            population_stops,
            ',',
        )

        mean_positions = []
        std_dev_positions = []
        mean_start_times = []
        mean_precedences = []
        println(file_name)
        for j = 1:length(burst_start_places)
            if length(burst_start_places[j]) > 5
                #println(burst_start_places[j])
                #println(burst_start_times[j])
                #println(j - 1, " ", length(burst_start_places[j]), " ", mean(burst_start_places[j]))
                push!(mean_positions, mean(burst_start_places[j]))
                push!(std_dev_positions, std(burst_start_places[j]))
                push!(mean_start_times, median(burst_start_times[j]))
            else
                push!(mean_positions, -1)
                push!(std_dev_positions, -1)
                push!(mean_start_times, -1)
            end
            temp_mean_precedences = []
            for k = 1:length(burst_start_places)
                if length(burst_start_precedences[j][k]) > 0
                    push!(temp_mean_precedences, mean(burst_start_precedences[j][k]))
                else
                    push!(temp_mean_precedences, -1)
                end
            end
            push!(mean_precedences, temp_mean_precedences)
        end
        println("\n\n")
        println("prec")
        println(mean_precedences[10][11])
        println(mean_precedences[11][10])
        println()

        order = sortperm(mean_positions)
        println(order)
        println(mean_positions[order])

        writedlm(
            string(params.BURST_RECORDS_FOLDER, params.RUNS[i], "/", file_name, ".mean_positions"),
            mean_positions,
            ',',
        )
        writedlm(
            string(params.BURST_RECORDS_FOLDER, params.RUNS[i], "/", file_name, ".std_dev_positions"),
            std_dev_positions,
            ',',
        )
        writedlm(
            string(params.BURST_RECORDS_FOLDER, params.RUNS[i], "/", file_name, ".mean_start_times"),
            mean_start_times,
            ',',
        )
        writedlm(
            string(params.BURST_RECORDS_FOLDER, params.RUNS[i], "/", file_name, ".mean_precedences"),
            mean_precedences,
            ',',
        )

    end
end

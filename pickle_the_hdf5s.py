import numpy as np
import h5py
import pickle
import math
from pathlib import Path

import params

def read_TE_and_surrogates(directory, file_pattern):

    # Use -1 to represent missing values (usually due to insufficient spikes for estimation)
    TE = -1 * np.ones((params.NUM_NODES, params.NUM_NODES))
    surrogates = -1 * np.ones((params.NUM_NODES, params.NUM_NODES, params.NUM_SURROGATES))
    burst_TE = -1 * np.ones((params.NUM_NODES, params.NUM_NODES))
    burst_surrogates = -1 * np.ones((params.NUM_NODES, params.NUM_NODES, params.NUM_SURROGATES))
    non_burst_surrogates = -1 * np.ones((params.NUM_NODES, params.NUM_NODES, params.NUM_SURROGATES))
    non_burst_TE = -1 * np.ones((params.NUM_NODES, params.NUM_NODES))

    pathlist = Path(directory).rglob(file_pattern)
    for filename in pathlist:
        print(filename)
        data_file = h5py.File(filename, 'r')

        for key in data_file.keys():
            # Check that there were enough spikes for estimation
            if np.count_nonzero(data_file[key]["surrogates"][()]) > 1:
                source_index = int(data_file[key]["source_index"][()])
                target_index = int(data_file[key]["target_index"][()])
                TE[source_index - 1, target_index - 1] = data_file[key]["TE"][()]

                if "TE_in_burst" in [at for at in data_file[key].keys()] and data_file[key]["num_in_burst"][()] > 0:
                    burst_TE[source_index - 1, target_index - 1] = (data_file[key]["TE_in_burst"][()]
                                                                    / data_file[key]["num_in_burst"][()])
                    burst_surrogates[source_index - 1, target_index - 1, :] = (data_file[key]["surrogates_in_burst"][()]/data_file[key]["num_in_burst"][()])

                if "TE_outside_burst" in [at for at in data_file[key].keys()] and data_file[key]["num_outside_burst"][()] != 0:
                    non_burst_TE[source_index - 1, target_index - 1] = (data_file[key]["TE_outside_burst"][()]
                                                                        / data_file[key]["num_outside_burst"][()])
                    non_burst_surrogates[source_index - 1, target_index - 1, :] = (data_file[key]["surrogates_outside_burst"][()]
                                                                                   / data_file[key]["num_outside_burst"][()])

                surrogates[source_index - 1, target_index - 1, :] = data_file[key]["surrogates"][()]

    return TE, surrogates, burst_TE, burst_surrogates, non_burst_TE, non_burst_surrogates


TE = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS, params.NUM_NODES, params.NUM_NODES))
surrogates = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS, params.NUM_NODES, params.NUM_NODES, params.NUM_SURROGATES))
burst_TE = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS, params.NUM_NODES, params.NUM_NODES))
burst_surrogates = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS, params.NUM_NODES, params.NUM_NODES, params.NUM_SURROGATES))
non_burst_TE = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS, params.NUM_NODES, params.NUM_NODES))
non_burst_surrogates = -1 * np.ones((params.NUM_RUNS, params.MAX_NUM_DAYS, params.NUM_NODES, params.NUM_NODES, params.NUM_SURROGATES))

for run_index in range(len(params.RUNS)):
    for file_index in range(len(params.FILES[run_index])):

        (files_TE, files_surrogates,
        files_burst_TE, files_burst_surrogates,
        files_non_burst_TE, files_non_burst_surrogates) = read_TE_and_surrogates(params.DATA_FOLDER + params.RUNS[run_index] + "/",
                                                                                params.FILES[run_index][file_index])

        TE[run_index, file_index, :, :] = files_TE
        surrogates[run_index, file_index, :, :, :] = files_surrogates
        burst_TE[run_index, file_index, :, :] = files_burst_TE
        burst_surrogates[run_index, file_index, :, :] = files_burst_surrogates
        non_burst_TE[run_index, file_index, :, :] = files_non_burst_TE
        non_burst_surrogates[run_index, file_index, :, :] = files_non_burst_surrogates


pickle_file = open(params.PICKLED_DATA_FILE_NAME, "wb")
pickle.dump(TE, pickle_file)
pickle.dump(surrogates, pickle_file)
pickle.dump(burst_TE, pickle_file)
pickle.dump(burst_surrogates, pickle_file)
pickle.dump(non_burst_TE, pickle_file)
pickle.dump(non_burst_surrogates, pickle_file)

pickle_file.close()

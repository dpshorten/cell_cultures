import numpy as np
import h5py
from pathlib import Path
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import seaborn as sns
import scipy.stats
from scipy.stats import norm
import utils
import params
import pickle_the_hdf5s

NUM_SURROGATES = 100
NUM_NODES = 60
#DATA_FOLDER = "results_wagenaar/1-1/"
DATA_FOLDER = "results_full/1-3/"
DATA_FILE_NAMES = ["1-3-24.2-lx4ly1-locBursts2-*", "1-3-24.2-lx4ly2-locBursts2-*", "1-3-24.2-lx4ly3-locBursts2-*"]

FIGURE_FOLDER = "figures_wagenaar/2-2/"

TE = -1 * np.ones((len(DATA_FILE_NAMES), params.NUM_NODES, params.NUM_NODES))
surrogates = -1 * np.ones((len(DATA_FILE_NAMES), params.NUM_NODES, params.NUM_NODES, params.NUM_SURROGATES))
#burst_TE = -1 * np.ones((len(DATA_FILE_NAMES), params.NUM_NODES, params.NUM_NODES))
#burst_surrogates = -1 * np.ones((len(DATA_FILE_NAMES), params.NUM_NODES, params.NUM_NODES, params.NUM_SURROGATES))
#non_burst_TE = -1 * np.ones((len(DATA_FILE_NAMES), params.NUM_NODES, params.NUM_NODES))
#non_burst_surrogates = -1 * np.ones((len(DATA_FILE_NAMES), params.NUM_NODES, params.NUM_NODES, params.NUM_SURROGATES))

for file_index in range(len(DATA_FILE_NAMES)):

    (files_TE, files_surrogates,
    temp1, temp2,
    temp3, temp4) = pickle_the_hdf5s.read_TE_and_surrogates(DATA_FOLDER, DATA_FILE_NAMES[file_index])

    TE[file_index, :, :] = 2.5e4 * files_TE
    surrogates[file_index, :, :, :] = 2.5e4 * files_surrogates

    print(TE[file_index, :, :])

#quit()

# for file_arr in surrogates:
#     print(len(file_arr))
#     if len(file_arr) > 0:
#         file_arr = np.mean(file_arr, axis = 1)
# for i in range(len(TE)):
#     for j in range(len(TE[i])):
#         TE[i][j] -= surrogates[i][j]

print()
for k in range(TE.shape[0]):
    for i in range(TE.shape[1]):
        for j in range(TE.shape[2]):
            surrogate_mean = np.mean(surrogates[k, i, j, :])
            surrogate_std = np.std(surrogates[k, i, j, :])
            if surrogate_std > 0:
                p = 1 - norm.cdf(TE[k, i, j], loc = surrogate_mean, scale = surrogate_std)
                if p > params.P_CUTOFF:
                    TE[k, i, j] = 0
                    surrogates[k, i, j, :] = np.zeros(surrogates.shape[3])

    print(k, np.count_nonzero(TE[k]))

surrogates = np.array(surrogates)
TE = np.array(TE)
surrogates = np.mean(surrogates, axis = 3)
TE -= surrogates

print(TE.shape)

#print(TE[2, 0:10])
#@print(TE[3, 0:10])

for i in range(len(TE) - 1):
    print("mean at ", i, " ", np.mean(TE[i, :, :]))
    print("mean surrogates ", i, " ", np.mean(surrogates[i, :, :]))
    print("mean at ", i + 1, " ", np.mean(TE[i + 1, :, :]))
    print("std at ", i, " ", np.std(TE[i, :, :]))
    print("std at ", i + 1, " ", np.std(TE[i + 1, :, :]))
    #print(len(TE[i]))
    print(scipy.stats.ttest_rel(TE[i, :, :], TE[i + 1, :, :], axis = None,
           alternative = 'less')[1])
    print("\n")


#TE.insert(1, [])
#TE.insert(3, [])
#TE.insert(5, [])
#TE.insert(7, [])
#TE.insert(9, [])
#surrogates.insert(0, [])
#surrogates.insert(2, [])
#surrogates.insert(4, [])
#surrogates.insert(6, [])
#surrogates.insert(8, [])
#surrogates.insert(10, [])

boxprops = dict(linewidth=1, color='red')
medianprops = dict(linewidth=1, color='red')
whiskerprops = dict(linewidth=1, color='red')
sboxprops = dict(linewidth=1, color='green')
smedianprops = dict(linewidth=1, color='green')
swhiskerprops = dict(linewidth=1, color='green')
plt.boxplot(np.transpose(surrogates), showfliers = False, boxprops = sboxprops, medianprops = smedianprops, whiskerprops = swhiskerprops)
#plt.boxplot(np.transpose(TE), showfliers = False, boxprops = boxprops, medianprops = medianprops, whiskerprops = whiskerprops)
#plt.ylim([-1.2, 1.2])
#plt.savefig(FIGURE_FOLDER + "2-2-33.2_vals" + ".png")
plt.show()



# mean_surrogates = np.mean(surrogates, axis = 2)
#
# corrected_TE = TE - mean_surrogates
# corrected_TE *= 1e3
# print(corrected_TE[1:5, 1:5])
# #sns.heatmap(corrected_TE, vmin = 0, vmax = 0.1, cmap = 'viridis')
# sns.heatmap(corrected_TE, vmin = 1e-9, cmap = 'viridis', norm = clr.LogNorm())

#plt.show()


#plt.clf()
